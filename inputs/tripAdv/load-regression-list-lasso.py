# Load the Pandas libraries with alias 'pd' 
import pandas as pd 
import glob

################################################## sklearn
import matplotlib.pyplot as plt 
import numpy as np 
from sklearn import datasets, linear_model, metrics 
##################################################
    
# Read data from file 'filename.csv' 
# (in the same directory that your python process is based)
# Control delimiters, rows, column names with read_csv (see later) 
fc = open("fileCoeff4-lasso-norm.csv","w+")

for file in glob.glob("FileUtenti/*.csv"):
   data = pd.read_csv(file) 


# put the original column names in a python list
   original_headers = list(data.columns.values)

# remove the non-numeric columns
   df = data._get_numeric_data()

# put the numeric column names in a python list
   numeric_headers = list(df.columns.values)

# create a numpy array with the numeric values for input into scikit-learn
   numpy_array = df.as_matrix()

   X = numpy_array[:,3:8]
   y = numpy_array[:,2]
   
   print(y)
   #y.savetxt('test.out', x, delimiter=',')
   
   print(X)



#################################################################################### sklearn

# splitting X and y into training and testing sets 
   from sklearn.model_selection import train_test_split 
   X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1) 
   
  

# create linear regression object 
   reg = linear_model.LinearRegression(fit_intercept=False) 
   
   lin = linear_model.Lasso(alpha=0.0001,precompute=True,max_iter=1000,
            positive=True, random_state=9999, selection='random', fit_intercept=False)
   lin.fit(X_train, y_train)



# predicting and schecking
   #y_pred = reg.predict(X_test)
   y_pred = lin.predict(X_test)

   #from sklearn.model_selection import r2_score
   #score = r2_score(y_test,y_pred)


# regression coefficients 
   #print('Coefficients: ', reg.coef_)
   print('Coefficients: ', lin.coef_)

   #print(reg.coef_)
   #print('Intercept', reg.intercept_) 
   print('Intercept', lin.intercept_)


   
# variance score: 1 means perfect prediction 
   print('Variance score: {}'.format(lin.score(X_test, y_test))) 
   
   print('A', y_test.flatten())
   print('P', y_pred.flatten())

   sum = 0
   for i in range( 0, lin.coef_.size ):
     sum = sum + lin.coef_[i]
   
   #print("dim np",reg.coef_.size)
   s = ""
   for i in range( 0, lin.coef_.size ):
     s = s + "%1.3f" % ( lin.coef_[i]/sum )
     if i < (lin.coef_.size -1 ):
       s = s + ","
   
   s = s + "\n"
   #s = s + "%1.3f" % lin.intercept_ + "\n"
   print(s)
   fc.write(s)

fc.close()




