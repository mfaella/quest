#!/bin/bash

# input: file.csv and dir
# output: set of File-$users.csv in dir

listaUtenti=$(cat $1 | sed '1d' |  awk 'BEGIN { FS = "," } ; { print $2 }' | sort | uniq)
#echo "lista utenti"


for utente in $listaUtenti
do 
  #echo $utente
  n=$(grep -c "^[0-9][0-9]*,$utente," $1) 
  echo $n
  if [ $n -gt 4 ]
  then
    echo "file generation"
    grep "^[0-9][0-9]*,$utente," $1 > $2/File-$utente.csv
  fi
 
  
done
