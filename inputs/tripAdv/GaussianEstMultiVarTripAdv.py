import pandas as pd 
import numpy as np
from scipy import stats
from sklearn import mixture
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

data = pd.read_csv("fileCoeff4-lasso-norm.csv") 

print(data)

#mu=np.array([1,10,20])
#sigma=np.matrix([[4,10,0],[10,25,0],[0,0,100]])
#data=np.random.multivariate_normal(mu,sigma,1000)
#values = data.T

clf = mixture.GaussianMixture(n_components=1, covariance_type='full')
clf.fit(data)

print("means:")
print(clf.means_)

print("covariances:")
print(clf.covariances_)

print("weights:")
print(clf.weights_)

print("converged")
print(clf.converged_)

#kde = stats.gaussian_kde(values)
#density = kde(values)

#print(density)
#print(kde)

#fig, ax = plt.subplots(subplot_kw=dict(projection='3d'))
#x, y, z = values
#ax.scatter(x, y, z, c=density)
#plt.show()
