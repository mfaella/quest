README

- List of items:

Script: TripAdvCalculator.sh

It generates the items. One for each Hotel.
Selected fields: 5-12

Hotel ID, number of reviews, Prize, Overall Rating,Value Rating,Rooms Rating,Location Rating,Cleanliness Rating,Front Desk Rating,Service Rating,Business Service Rating

mean values are provided


Input: TripAdvFull.csv
Output: itemsHotel.csv


- Script: TripAdvUsers TripAdvFull.csv dir

It generates csv files in dir, one for each user
These files are used to extract the Oracle model (see python dir)

Python:

- multilinear regression: load-regression-list.py

- multivariate: GaussianEstMultiVarTripAdv.py






