#!/bin/bash

#input File: datiTripAdvFull.csv 
csvFile=$1

#output File: itemsHotel.csv 
outFile=$2

# min number of reviews
minRev=$3

listaHotel=$(cat $csvFile | sed '1d' |  awk 'BEGIN { FS = "," } ; { print $1 }' | sort | uniq)

#It generates the item list
# Hotel ID, Number reviews, Prize, Overall Rating,Value Rating,Rooms Rating,Location Rating,Cleanliness Rating,Front Desk Rating,Service Rating,Business Service Rating
# mean values are provided


for hotel in $listaHotel
do 
  Nrev=$(grep "^$hotel" $csvFile | wc -l)
  
  if [ $Nrev -ge $minRev ] 
  then
    grep "^$hotel" $csvFile | 
     awk -v var=$Nrev 'BEGIN { FS = "," } ; {for (i=5;i<=NF;i++){a[i]+=$i;} a[3]+=$3; } 
       END {printf $1 ","; printf "%d,", var; printf "%d,", a[3]/NR; for (i=5;i<=NF;i++){printf "%.1f", a[i]/NR; if( i<12) printf ","};printf "\n"}' >> $2

  fi

  
done
