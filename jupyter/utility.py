from collections import defaultdict

import pandas as pd
import numpy as np
import re


"""Return the name of family dataset using regex
:returns: The name of family dataset
"""
def get_family_name(str):
    regex = r'housing|synthetic|trip|beer'
    matches = re.search(regex, str)
    return matches.group(0)


"""Return the count of items valued in the experiment using regex
:returns: The count of items valued in the experiment
"""
def get_item_count(str):
    regex = r'[0-9]+'
    matches = re.search(regex, str)
    return matches.group(0)


"""Return the types of generator and prior distributions using regex
:returns: The types of generator and prior distributions
"""
def get_generator_prior(str):
    regex = r'generator_uniform_prior_uniform|generator_uniform_prior_normal|generator_normal_prior_uniform|' \
            r'generator_normal_prior_normal'
    matches = re.search(regex, str)
    return matches.group(0)


"""Return a dictionary with vois name
:returns: A dictionary with vois name
"""
def get_voi_name():
    return {"RNDs": "Random", "Vals": "$EBI_{val}$", "UsEs": "$EBI_{hb}$", "TTEs": "$EBI_{ht}$", "Prbs": "$EBI_{prb}$", "ReVs": "$EBI_{rval}$", "Tp2": "TopTwo"}


"""Return a dictionary with filters name
:returns: A dictionary with filters name
"""
def get_filter_name():
    return {"Con": "Consecutives", "SqR": "TopSqRoot", "TWA": "TopWithAll", "Tp2": "TopTwo", "All": "NoFilter"}


"""Return a dictionary with finders name
:returns: A dictionary with finders name
"""
def get_finder_name():
    return {"smart": "Smart Max Finder", "simple": "Simple Max Finder", "sr": "Successive Rejects",
            "sh": "Sequential Halving"}


"""Return a dictionary with recommenders name
:returns: A dictionary with recommenders name
"""
def get_rec_name():
    return {"Prbr": "$rec_{prb}$", "AbRr": "$rec_\mathit{regret}$", "ReVr": "$rec_\mathit{rval}$", "Valr": "$rec_{val}$"}


"""Return a dictionary with vois style
:returns: A dictionary with vois style
"""
def get_voi_style():
    return {"RNDs": ':', "Vals": '--', "UsEs": '-.', "TTEs": '-', "Prbs": '-', "ReVs": '-', "Tp2": "-."}


"""Return a dictionary with filters style
:returns: A dictionary with filters style
"""
def get_filter_style():
    return {"Tp2": ':', "Con": '-.', "SqR": '-', "TWA": '--', "All": '--'}


"""Return a dictionary with finders style
:returns: A dictionary with finders style
"""
def get_finder_style():
    return {"smart": ":", "simple": "-.", "sr": "-", "sh": "--"}


"""Return a dictionary with recommenders style
:returns: A dictionary with recommenders style
"""
def get_rec_style():
    return {"Prbr": ':', "AbRr": '--', "ReVr": "-.", "Valr": '-'}


"""Read a list of input files and return their content
:param paths: A dictionary having number of items as key and path as value
:returns: A list of filters, a list of recommenders, a list of selectors, a dictionary having number of items as key and
        content of respective input file as value
"""
def read_input_files(paths):
    filters = []
    recommenders = []
    selectors = []
    output = {}

    for k, v in paths.items():
        output[k] = pd.read_csv(v)
        filters_read_from_output = output[k]["filter"].unique()
        filters = np.concatenate((filters, filters_read_from_output))
            
        if len(recommenders) == 0:
            recommenders = output[k]["recommender"].unique()

        if len(selectors) == 0:
            selectors = output[k]["selector"].unique()
            # In order to have random, if it is present in selectors, as last element
            rnd_index_list = np.where(selectors == ' RNDs')[0]  # a list containing indexes of RNDs selector
            if rnd_index_list.size != 0:
                rnd_index = rnd_index_list[0]
                selectors_without_rnd = np.delete(selectors, rnd_index)
                selectors = np.append(selectors_without_rnd, ' RNDs')
    return list(set(filters)), recommenders, selectors, output


"""Warning! It doesn't works for time plot. Return the count of user
:param data: The content of input file
:returns: The count of user
"""
def get_user_count(data):
    user_count = 0
    for k, v in data.items():
        user_count = data[k]['user'].nunique()
    return user_count


"""Warning! It doesn't works for time plot. Return the frequency of true tops
:param filters: A list of filters
:param recommenders: A list of recommenders
:param selectors: A list of selectors
:param data: The content of input file
:returns: The frequency of true tops
"""
def get_frequency_of_true_tops(filters, recommenders, selectors, data):
    count = {}
    vois_count = len(selectors)
    filters_count = len(filters)
    recommenders_count = len(recommenders)
    query_count = 0
    user_count = 0
    for k, v in data.items():
        query_count = data[k]['query'].nunique()
        user_count = data[k]['user'].nunique()
        for top in data[k]['top']:
            count[top] = count.get(top, 0) + 1

        for top in data[k]['top'].unique():
            count[top] = 100 * count[top] / vois_count / filters_count / recommenders_count / query_count / user_count
    return count


"""Warning! It doesn't works for time plot. Return the list of queries
:param data: The content of input file
:returns: The list of queries
"""
def get_queries_list(data):
    queries_list = []
    for k, v in data.items():
        queries_list = data[k]['query'].unique()
    return queries_list


"""Warning! It doesn't works for time plot. Return a dictionary with voi time performance, a dictionary with global
   time performance and a dictionary with count of wrong recommendation
:param data: The content of input file
:returns: A dictionary with voi time performance, a dictionary with global time performance and a dictionary with count
          of wrong recommendation
"""
def calculate_voi_and_global_performance(data):
    voiPerformanceDictPerKeyData = defaultdict(list)
    globalPerformanceDictPerKeyData = defaultdict(list)
    wrongRecCountDictPerKeyData = defaultdict(list)
    for k, v in data.items():
        wrongRecCountDict = defaultdict(list)
        voiPerformanceDict = defaultdict(list)
        globalPerformanceDict = defaultdict(list)
        for index, row in data[k].iterrows():
            key = (row["recommender"], row["filter"], row["selector"], row["query"])
            subKey = (row["recommender"], row["filter"], row["selector"])
            wrongRecCountDict[key].append(0)
            if row["recUtil"] != row["topUtil"]:
                wrongRecCountDict[key].append(1)

            voiPerformanceDict[key].append(row["time"])
            globalPerformanceDict[subKey].append(row["time"])

        voiPerformanceDictPerKeyData[k] = voiPerformanceDict
        globalPerformanceDictPerKeyData[k] = globalPerformanceDict
        wrongRecCountDictPerKeyData[k] = wrongRecCountDict
    return voiPerformanceDictPerKeyData, globalPerformanceDictPerKeyData, wrongRecCountDictPerKeyData


"""Warning! It doesn't works for time plot. Return a dictionary with regret (a.k.a. accuracy) for each recommendation
:param data: The content of input file
:returns: A dictionary with regret for each recommendation
"""
def calculate_regret_for_each_recommendation(data):
    voiAccuracyDictPerKeyData = defaultdict(list)
    for k, v in data.items():
        voiAccuracyDict = defaultdict(list)
        for index, row in data[k].iterrows():
            key = (row["recommender"], row["filter"], row["selector"], row["query"])

            voiAccuracyDict[key].append(abs(row["recUtil"]-row["topUtil"]))
        voiAccuracyDictPerKeyData[k] = voiAccuracyDict
    return voiAccuracyDictPerKeyData


"""Warning! It doesn't works for time plot. Return a dictionary with relative utility for each recommendation
:param data: The content of input file
:returns: A dictionary with relative utility for each recommendation
"""
def calculate_relative_utility_for_each_recommendation(data):
    voiAccuracyDictPerKeyData = defaultdict(list)
    for k, v in data.items():
        voiAccuracyDict = defaultdict(list)
        for index, row in data[k].iterrows():
            key = (row["recommender"], row["filter"], row["selector"], row["query"])

            voiAccuracyDict[key].append(((row["recUtil"]-row["bottomUtil"])/(row["topUtil"]-row["bottomUtil"])))
        voiAccuracyDictPerKeyData[k] = voiAccuracyDict
    return voiAccuracyDictPerKeyData


