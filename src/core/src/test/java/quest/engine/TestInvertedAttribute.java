package quest.engine;

import static org.junit.Assert.*;

import quest.engine.attribute.Attribute;
import quest.engine.attribute.BoundedSigmoid;
import quest.engine.attribute.Inverted;

public class TestInvertedAttribute {
    private Attribute attr = new Inverted(new BoundedSigmoid("Test", 0, 1, "cm",0));
    private static final double DELTA = 0.001;

    // @Test
    public void plotAttribute() {
	final double[] params = { 0.9 };
	for (double x=0; x<=1; x+=0.02) {
	    double y = attr.eval(params, 0, x);
	    System.out.println(x + ", " + y);	   
	}
	assertTrue(true);
    }
}
