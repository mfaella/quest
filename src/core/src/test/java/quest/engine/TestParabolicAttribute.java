package quest.engine;

import static org.junit.Assert.*;

import org.junit.Test;

import quest.engine.attribute.Parabolic;

public class TestParabolicAttribute {
    private Parabolic attr = new Parabolic("Test", 0, 1, "cm",0);
    private static final double DELTA = 0.001;

    @Test
    public void testVertex() {
	for (double param=0; param<=1; param+=0.01) {
	    double y = attr.eval(new double[] {param}, 0, param);
	    assertEquals("Wrong vertex with param=" + param, 1, y, DELTA);
	}
    }

    // @Test
    public void plotAttribute() {
	final double[] param = { 0.5 };
	for (double x=0; x<=1; x+=0.01) {
	    double y = attr.eval(param, 0, x);
	    System.out.println(x + ", " + y);	   
	}
	assertTrue(true);
    }
}
