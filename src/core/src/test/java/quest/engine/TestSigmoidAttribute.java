package quest.engine;

import static org.junit.Assert.*;

import org.junit.Test;

import quest.engine.attribute.BoundedSigmoid;

public class TestSigmoidAttribute {
    private BoundedSigmoid attr = new BoundedSigmoid("Test", 0, 1, "cm",0);
    private static final double DELTA = 0.001;

    @Test
    public void testExtremes() {
	for (double param=0.01; param<=1; param+=0.01) {
	    double y = attr.eval(new double[] { param }, 0, 0);
	    assertEquals("Wrong left extreme with param=" + param, 0, y, DELTA);
	    y = attr.eval(new double[] { param}, 0, 1);
	    assertEquals("Wrong right extreme with param=" + param, 1, y, DELTA);
	}
    }

    @Test
    public void testDominance() {
	final double step = 0.05;
	for (double param=0.01; param<1-step; param+=step) {
	    for (double x=0; x<=param; x+=0.01) {
		double y1 = attr.eval(new double[] { param }, 0, x);
		double y2 = attr.eval(new double[] { param + step }, 0, x);
		assertTrue("Curve with higher parameter not lower than other (param=" + param +
			   ", x=" + x + ")",
			   y1 >= y2);
	    }
	}
    }

    @Test
    public void plotAttribute() {
	final double[] params = { 0.9 };
	for (double x=0; x<=1; x+=0.02) {
	    double y = attr.eval(params, 0, x);
	    System.out.println(x + ", " + y);	   
	}
	assertTrue(true);
    }
}
