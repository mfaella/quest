package quest.exp;

import bai.core.*;
import bai.sampler.*;

import java.util.*;
import org.apache.commons.math3.distribution.*;

public class Generators {

    private static final Random random = new Random();
    private static final Function identity = Function.make(a -> a[0], 1);
    
    public static Generator randomGaussians(final double minSd, final double maxSd) {
	return (int numerosity,	List<Function> functions, List<Sampler> samplers, List<Moments> moments) ->
	    {
		final double MAX_MEAN = 1;
		
		int maxId = 0;
		double maxMean = Double.NEGATIVE_INFINITY;

		for (int i=0; i<numerosity; i++) {
		    Moments m = new Moments();
		    m.mean = MAX_MEAN * random.nextDouble();
		    m.sd   = minSd + (maxSd - minSd) * random.nextDouble();
		    moments.add(m);

		    samplers.add(new ApacheSampler(new NormalDistribution(m.mean, m.sd)));  
		    if (m.mean > maxMean) {
			maxMean = m.mean;
			maxId = i;
		    }
		    functions.add(identity);
		}
		return maxId;
	    };
    }

    public static Generator randomBernoulli() {
	return (int numerosity,	List<Function> functions, List<Sampler> samplers, List<Moments> moments) ->
	    {
		int maxId = 0;
		double maxMean = Double.NEGATIVE_INFINITY;
		double[] values = { 0, 1 }, probs = { 0, 0 };
		
		for (int i=0; i<numerosity; i++) {
		    Moments m = new Moments();

		    probs[1] = m.mean = random.nextDouble();
		    probs[0] = 1 - probs[1];
		    m.sd   = Math.sqrt(probs[0] * probs[1]);
		    moments.add(m);

		    samplers.add(new ApacheSampler(new EnumeratedRealDistribution(values, probs)));  
		    if (m.mean > maxMean) {
			maxMean = m.mean;
			maxId = i;
		    }
		    functions.add(identity);
		}
		return maxId;
	    };
    }


    public static Generator fixedH2() {
	return (int numerosity,	List<Function> functions, List<Sampler> samplers, List<Moments> moments) ->
	    {
		final double minSd = 2, maxSd = 10;

		// Top arm
		Moments m1 = new Moments();
		m1.mean = 1;
		m1.sd = minSd + (maxSd - minSd) * random.nextDouble();
		moments.add(m1);
		samplers.add(new ApacheSampler(new NormalDistribution(m1.mean, m1.sd)));
		functions.add(identity);
		
		for (int i=1; i<numerosity; i++) {
		    Moments m = new Moments();
		    m.mean = 0;
		    m.sd   = minSd + (maxSd - minSd) * random.nextDouble();
		    moments.add(m);  
		    samplers.add(new ApacheSampler(new NormalDistribution(m.mean, m.sd)));
		    functions.add(identity);
		}
		return 0;
	    };
    }

}
