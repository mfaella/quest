package quest.exp;

import java.util.*;
import java.util.function.Supplier;
import bai.core.*;
import bai.finder.*;
import bai.util.*;
import bai.sampler.*;
import org.apache.commons.math3.distribution.*;

import java.io.PrintStream;

/**
 * Performs the experiments for the AAAI 2020 paper.
 *
 */
public class AAAI20
{
    private static final Random random = new Random();
    private static final Function identity = Function.make(a -> a[0], 1);
    private static int iterations = 0;
    
    private static final double SMALL_MIN_SD = 0.01, SMALL_MAX_SD = 0.1,
	MEDIUM_MIN_SD = 0.1, MEDIUM_MAX_SD = 0.5,
	LARGE_MIN_SD = 0.5, LARGE_MAX_SD = 1.5;


    private static void printUsage() {
	System.out.println("Usage: qest [rade|bern|small|medium|large|numerosity|H2|mult] [<output filename>]");
    }
    
    public static void main( String[] args ) throws java.io.IOException
    {
	if (args.length==0) {
	    printUsage();
	    return;
    	}
	
    	if (args.length>1) {
    		PrintStream outfile = new PrintStream(args[1]);
    		System.setOut(outfile);
    	}

	final int[] numerositiesFix = { 40 },
	    budgetVar = { 5000, 10000, 15000, 20000 },
	    budgetFix = { 15000 };

	iterations = 100000;
	
	switch (args[0]) {
	case "rade":
	    System.out.println("Random Rademachers.");
	    testRandomRademacher();
	    break;
	case "bern":
	    System.out.println("Random Bernoullis.");
	    testOnGenerator(Generators.randomBernoulli(), numerositiesFix, budgetVar);
	    break;
	case "small":
	    System.out.println("Random univariate Gaussians.");
	    testOnGenerator(Generators.randomGaussians(SMALL_MIN_SD, SMALL_MAX_SD),
			    numerositiesFix, budgetVar);
	    break;
	case "medium":
	    System.out.println("Random univariate Gaussians.");
	    testOnGenerator(Generators.randomGaussians(MEDIUM_MIN_SD, MEDIUM_MAX_SD),
			    numerositiesFix, budgetVar);
	    break;
	case "large":
	    System.out.println("Random univariate Gaussians.");
	    testOnGenerator(Generators.randomGaussians(LARGE_MIN_SD, LARGE_MAX_SD),
			    numerositiesFix, budgetVar);	    
	    break;
	case "numerosity":
	    int[] numerositiesVar = { 20, 40, 80, 160, 320 }, budgets = { 2000, 4000, 8000, 16000, 32000 };	    
	    System.out.println("Random Bernoullis with varying number of arms.");
	    if (args.length>2) {		
		int k = Integer.valueOf(args[2]);
		testOnGenerator(Generators.randomBernoulli(),
				Arrays.copyOfRange(numerositiesVar, k, k+1),
				Arrays.copyOfRange(budgets, k, k+1));
	    } else {
		testOnGenerator(Generators.randomBernoulli(), numerositiesVar, budgets);
	    }
	    // Old version: on Gaussians with medium variance
	    // System.out.println("Random univariate Gaussians.");
	    // testOnGenerator(Generators.randomGaussians(MEDIUM_MIN_SD, MEDIUM_MAX_SD), numerositiesVar, budgets);
	    break;
	case "H2":
	    System.out.println("Random fixed-H2 Gaussians.");
	    testOnGenerator(Generators.fixedH2(), numerositiesFix, budgetVar);
	    break;
	case "mult":
	    System.out.println("Varying multiplier.");
	    testMultiplier(Generators.randomGaussians(MEDIUM_MIN_SD, MEDIUM_MAX_SD));
	    break;
	case "test":
	    System.out.println("Internal test.");
	    testFixedRademacherGabillon11();
	    break;
	default:
	    System.out.println("Unrecognized option.");
	    printUsage();
	}
    }

    /** Experiment with varying gamma VBR confidence parameter. */
    public static void testMultiplier(Generator generator) {
	double[] multipliers = { 0, 0.5, 1, 2, 4, 8 };
	final int[] numerosities = { 40 };
	final int[] sampleCounts = { 15000 };
	
    	FinderComparison comp = new FinderComparison("SR", "Unif", "VBR0", "VBR0.5", "VBR1", "VBR2", "VBR4", "VBR8");
	Supplier<MaxFinder>[] roster = new Supplier[multipliers.length + 2];
	
    	System.out.println("Iterations: " + iterations);	
    	System.out.println("Algo\tTime\tError\tRelErr\tFNum\tBudget\tH1\tH2\tHsigma");

    	for (int numerosity : numerosities) {
	    for (int maxSamples : sampleCounts) {
		String basicMessage = "\t" + numerosity + "\t" + maxSamples;
		for (int attempt=0; attempt<iterations; attempt++) {
		    // Populate functions
		    List<Function> functions = new ArrayList<>(numerosity);
		    List<Sampler> samplers = new ArrayList<>(numerosity);
		    List<Moments> moments = new ArrayList<>(numerosity);
		    int maxId = generator.generate(numerosity, functions, samplers, moments);
		    double maxMean = moments.get(maxId).mean;
			
		    // Input statistics
		    moments.sort(Comparator.comparingDouble(m -> m.mean));
		    // The closest to the top
		    double deltaStar = maxMean - moments.get(numerosity-2).mean;
		    double sigma1squared = moments.get(numerosity-1).sd * moments.get(numerosity-1).sd;
			double H1 = 1/(deltaStar*deltaStar),
			    H2 = 1/(deltaStar*deltaStar),
			    Hsigma = Double.NEGATIVE_INFINITY;
			// This loop skips the top
			for (int i=0; i<numerosity-1; i++) {
			    double delta = maxMean - moments.get(i).mean;
			    H1 += 1/(delta*delta);
			    double term = (numerosity - i) / (delta*delta);
			    double sigmaTerm = (sigma1squared +  moments.get(i).sd*moments.get(i).sd ) /
				(delta*delta);
			    if (term > H2)
				H2 = term;
			    if (sigmaTerm > Hsigma)
				Hsigma = sigmaTerm;
			}
			String message = basicMessage + "\t" + H1 + "\t" + H2 + "\t" + Hsigma;
			roster[0] = () -> new SRMaxFinder(samplers, functions, maxSamples);
			roster[1] = () -> new SimpleMaxFinder(samplers, functions, maxSamples);
			int i = 2;
			for (double sdMultiplier: multipliers) {
			    // message = basicMessage + "\t" + sdMultiplier;
			    roster[i++] = () ->
				SmartMaxFinder.newFixedBudgetIid(samplers, functions, maxSamples, sdMultiplier);
			}
			comp.addComparison(maxId, maxMean, message, roster);
		    }		
    		}
    	}
    }

    
    public static void testFixedRademacher() {
	// final int attempts = 20000;
        final double sdMultiplier = 2;
	final int[] sampleCounts = { 800, 1600, 3200 };

	double[][] values = { {0, 1},     {0.45, 0.45}, {0.25, 0.65}, {0, 0.9},
			      {0.4, 0.6}, {0.45, 0.45}, {0.35, 0.55}, {0.25, 0.65},
	                      {0, 1},      {0.45, 0.45}, {0.25, 0.65}, {0, 0.9},
	                      {0.4, 0.6}, {0.45, 0.45}, {0.35, 0.55}, {0.25, 0.65}};
	double[] probs = {0.5, 0.5};	
	List<Function> functions = new ArrayList<>(values.length);
	List<Sampler> samplers = new ArrayList<>(values.length);
	for (int i=0; i<values.length; i++) {
	    // so-called Rademacher distribution: two values with equal prob
	    samplers.add(new ApacheSampler(new EnumeratedRealDistribution(values[i], probs)));  
	    functions.add(identity);
	}	
	double maxMean = 0.5;
    	FinderComparison comp = new FinderComparison("SR", "SH", "UCBE", "OUCBE", "Unif", "GapEV", "VBR");

    	System.out.println("Attempts: " + iterations);
    	System.out.println("Algo\tTime\tError\tRelErr\tFNum\tBudget\tGamma");

	for (int maxSamples : sampleCounts) {
	    String message = "\t" + values.length + "\t" + maxSamples + "\t" + sdMultiplier;
	    for (int attempt=0; attempt<iterations; attempt++) {
		Supplier<MaxFinder> sr = () -> new SRMaxFinder(samplers, functions, maxSamples);
		Supplier<MaxFinder> hr = () -> new SHMaxFinder(samplers, functions, maxSamples);
		Supplier<MaxFinder> ucbe = () -> new AdaUCBEMaxFinder(samplers, functions, maxSamples, 1.0);
		Supplier<MaxFinder> optucbe = () -> new OptimizedAdaUCBEMaxFinder(samplers, functions, maxSamples, 1.0);
		Supplier<MaxFinder> naive = () -> new SimpleMaxFinder(samplers, functions, maxSamples);
		Supplier<MaxFinder> gap = () -> new GapEVMaxFinder(samplers, functions, maxSamples);
		Supplier<MaxFinder> adaptive = () ->
		    SmartMaxFinder.newFixedBudgetIid(samplers, functions, maxSamples, sdMultiplier);
		comp.addComparison(id -> id%4 == 0, maxMean, message, sr, hr, ucbe, optucbe, naive, gap, adaptive);
	    }
	}
    }


    public static void testFixedRademacherGabillon11() {
	iterations = 10000;
        final double sdMultiplier = 1;
	final int[] sampleCounts = { 2000 };

	double[][] values = { {0.47, 0.47}, {0.25, 0.65}, {0, 0.9},
			      {0.35, 0.55},  {0.3, 0.6} };
	double[] probs = {0.5, 0.5};	
	List<Function> functions = new ArrayList<>(values.length);
	List<Sampler> samplers = new ArrayList<>(values.length);
	for (int i=0; i<values.length; i++) {
	    // so-called Rademacher distribution: two values with equal prob
	    samplers.add(new ApacheSampler(new EnumeratedRealDistribution(values[i], probs)));  
	    functions.add(identity);
	}	
	double maxMean = 0.5;
    	FinderComparison comp = new FinderComparison("SR", "SH", "OUCBE", "Unif", "GapEV", "VBR");

    	System.out.println("Attempts: " + iterations);
    	System.out.println("Algo\tTime\tError\tRelErr\tFNum\tBudget\tGamma");

	for (int maxSamples : sampleCounts) {
	    String message = "\t" + values.length + "\t" + maxSamples + "\t" + sdMultiplier;
	    for (int attempt=0; attempt<iterations; attempt++) {
		Supplier<MaxFinder> sr = () -> new SRMaxFinder(samplers, functions, maxSamples);
		Supplier<MaxFinder> hr = () -> new SHMaxFinder(samplers, functions, maxSamples);
		Supplier<MaxFinder> ucbe = () -> new AdaUCBEMaxFinder(samplers, functions, maxSamples, 1.0);
		Supplier<MaxFinder> optucbe = () -> new OptimizedAdaUCBEMaxFinder(samplers, functions, maxSamples, 1.0);
		Supplier<MaxFinder> naive = () -> new SimpleMaxFinder(samplers, functions, maxSamples);
		Supplier<MaxFinder> gap = () -> new GapEVMaxFinder(samplers, functions, maxSamples);
		Supplier<MaxFinder> adaptive = () ->
		    SmartMaxFinder.newFixedBudgetIid(samplers, functions, maxSamples, sdMultiplier);
		comp.addComparison(id -> id%4 == 0, maxMean, message, sr, hr, /*ucbe,*/ optucbe, naive, gap, adaptive);
	    }
	}
    }


    
    public static void testRandomRademacher() {
    	// final int attempts = 10000;
    	final double sdMultiplier = 2;
    	final int[] sampleCounts = { 5000, 10000, 15000, 20000 };
    	final double MIN_VALUE = 0, MAX_VALUE = 1;
    	final int numerosity = 40;
    	final double[] values = new double[2], probs = {0.5, 0.5};	

    	FinderComparison comp = new FinderComparison("SR", "SH", "UCBE", "Unif", "GapEV", "VBR");

    	System.out.println("Attempts: " + iterations);
    	System.out.println("Algo\tTime\tError\tRelErr\tFNum\tBudget\tGamma");

    	for (int maxSamples : sampleCounts) {
    		String message = "\t" + numerosity + "\t" + maxSamples + "\t" + sdMultiplier;
    		for (int attempt=0; attempt<iterations; attempt++) {

    			List<Function> functions = new ArrayList<>(numerosity);
    			List<Sampler> samplers = new ArrayList<>(numerosity);
    			double maxMean = Double.NEGATIVE_INFINITY;
    			int maxId = 0;
    			for (int i=0; i<numerosity; i++) {
    				// so-called Rademacher distribution: two values with equal prob
    				values[0] = MIN_VALUE + (MAX_VALUE - MIN_VALUE) * random.nextDouble();
    				values[1] = MIN_VALUE + (MAX_VALUE - MIN_VALUE) * random.nextDouble();
    				samplers.add(new ApacheSampler(new EnumeratedRealDistribution(values, probs)));  
    				functions.add(identity);
    				double mean = (values[0] + values[1]) / 2;
    				if (mean > maxMean) {
    					maxMean = mean;
    					maxId = i;
    				}
    			}	

    			Supplier<MaxFinder> sr = () -> new SRMaxFinder(samplers, functions, maxSamples);
    			Supplier<MaxFinder> hr = () -> new SHMaxFinder(samplers, functions, maxSamples);
    			// Supplier<MaxFinder> ucbe = () -> new AdaUCBEMaxFinder(samplers, functions, maxSamples, 1.0);
    			Supplier<MaxFinder> optucbe = () -> new OptimizedAdaUCBEMaxFinder(samplers, functions, maxSamples, 1.0);
    			Supplier<MaxFinder> naive = () -> new SimpleMaxFinder(samplers, functions, maxSamples);
			Supplier<MaxFinder> gap = () -> new GapEVMaxFinder(samplers, functions, maxSamples);
    			Supplier<MaxFinder> adaptive = () ->
			    SmartMaxFinder.newFixedBudgetIid(samplers, functions, maxSamples, sdMultiplier);
    			comp.addComparison(maxId, maxMean, message, sr, hr, optucbe, naive, gap, adaptive);
    		}
    	}
    }


    public static void test() {
    	final int[] sampleCounts = { 10000 };
    	final double MIN_VALUE = 0, MAX_VALUE = 1;
    	final int numerosity = 40;
    	final double[] values = new double[2], probs = {0.5, 0.5};	

    	FinderComparison comp = new FinderComparison("Unif", "GapEV");

    	System.out.println("Attempts: " + iterations);
    	System.out.println("Algo\tTime\tError\tRelErr\tFNum\tBudget\tGamma");

	iterations = 1;
    	for (int maxSamples : sampleCounts) {
	    String message = "\t" + numerosity + "\t" + maxSamples;
	    for (int attempt=0; attempt<iterations; attempt++) {
		
		List<Function> functions = new ArrayList<>(numerosity);
		List<Sampler> samplers = new ArrayList<>(numerosity);
		double maxMean = Double.NEGATIVE_INFINITY;
		int maxId = 0;
		for (int i=0; i<numerosity; i++) {
		    // so-called Rademacher distribution: two values with equal prob
		    values[0] = MIN_VALUE + (MAX_VALUE - MIN_VALUE) * random.nextDouble();
		    values[1] = MIN_VALUE + (MAX_VALUE - MIN_VALUE) * random.nextDouble();
		    samplers.add(new ApacheSampler(new EnumeratedRealDistribution(values, probs)));  
		    functions.add(identity);
		    double mean = (values[0] + values[1]) / 2;
		    if (mean > maxMean) {
			maxMean = mean;
			maxId = i;
		    }
		}	
		Supplier<MaxFinder> naive = () -> new SimpleMaxFinder(samplers, functions, maxSamples);
		Supplier<MaxFinder> gap = () -> new GapEVMaxFinder(samplers, functions, maxSamples);
		comp.addComparison(maxId, maxMean, message, naive, gap);
	    }
    	}
    }

    
    public static void testOnGenerator(Generator generator, int[] numerosities, int[] sampleCounts) {
    	final double sdMultiplier = 2;

    	FinderComparison comp = new FinderComparison("SR", "SH", "UCBE", "GapEV", "Unif", "VBR");
    	System.out.println("Iterations: " + iterations);	
    	System.out.println("Algo\tTime\tError\tRelErr\tFNum\tBudget\tGamma\tH1\tH2\tHsigma");

	int n = numerosities.length > sampleCounts.length? numerosities.length : sampleCounts.length;
	for (int k=0; k<n; k++) {
	    int numerosity, maxSamples;
	    if (numerosities.length > 1) {
		numerosity = numerosities[k];
	    } else {
		numerosity = numerosities[0];
	    }
	    if (sampleCounts.length > 1) {
		maxSamples = sampleCounts[k];
	    } else {
		maxSamples = sampleCounts[0];
	    }
	    String basicMessage = "\t" + numerosity + "\t" + maxSamples + "\t" + sdMultiplier;

	    for (int attempt=0; attempt<iterations; attempt++) {
		// Populate functions
		List<Function> functions = new ArrayList<>(numerosity);
		List<Sampler> samplers = new ArrayList<>(numerosity);
		List<Moments> moments = new ArrayList<>(numerosity);
		
		int maxId = generator.generate(numerosity, functions, samplers, moments);
		double maxMean = moments.get(maxId).mean;

		// Input statistics
		moments.sort(Comparator.comparingDouble(m -> m.mean));
		// The closest to the top
		double deltaStar = maxMean - moments.get(numerosity-2).mean;
		double sigma1squared = moments.get(numerosity-1).sd * moments.get(numerosity-1).sd;
		double H1 = 1/(deltaStar*deltaStar),
		    H2 = 1/(deltaStar*deltaStar),
		    Hsigma = Double.NEGATIVE_INFINITY;
		// This loop skips the top
		for (int i=0; i<numerosity-1; i++) {
		    double delta = maxMean - moments.get(i).mean;
		    H1 += 1/(delta*delta);
		    double term = (numerosity - i) / (delta*delta);
		    double sigmaTerm = (sigma1squared +  moments.get(i).sd*moments.get(i).sd ) /
			(delta*delta);
		    if (term > H2)
			H2 = term;
		    if (sigmaTerm > Hsigma)
			Hsigma = sigmaTerm;
		}
		String message = basicMessage + "\t" + H1 + "\t" + H2 + "\t" + Hsigma;

		Supplier<MaxFinder> sr = () -> new SRMaxFinder(samplers, functions, maxSamples);
		Supplier<MaxFinder> hr = () -> new SHMaxFinder(samplers, functions, maxSamples);
		// Supplier<MaxFinder> ucbe = () -> new AdaUCBEMaxFinder(samplers, functions, maxSamples, 1.0);
		Supplier<MaxFinder> optucbe = () -> new OptimizedAdaUCBEMaxFinder(samplers, functions, maxSamples, 1.0);
		Supplier<MaxFinder> gap = () -> new GapEVMaxFinder(samplers, functions, maxSamples);
		Supplier<MaxFinder> naive = () -> new SimpleMaxFinder(samplers, functions, maxSamples);
		Supplier<MaxFinder> adaptive = () ->
		    SmartMaxFinder.newFixedBudgetIid(samplers, functions, maxSamples, sdMultiplier);
		comp.addComparison(maxId, maxMean, message, sr, hr, optucbe, gap, naive, adaptive);
	    }		
	}
    }
}
