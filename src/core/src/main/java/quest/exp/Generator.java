package quest.exp;

import java.util.*;
import bai.core.*;
import bai.sampler.*;

public interface Generator {
    int generate(int numerosity,
		 List<Function> functions,
		 List<Sampler> samplers,
		 List<Moments> moments);
}

    
