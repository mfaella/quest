package quest.engine;

import quest.engine.filter.QueryFilter;
import quest.engine.model.Belief;
import quest.engine.selector.QuerySelector;
import quest.engine.recommender.Recommender;

/** The specifications (configuration) for an elicitation algorithm.
 *
 * Example of EngineSpecs instantiation:
 * <code>EngineSpecs specs = new EngineSpecs(DensityParser.parseUniform(domain.dim, NULL),
 *                                        new MinRegret(...),
 *                                        new TopSquareRoot(domain),
 *                                        new MinTopEntropy(...));
 *                                        </code>
 */
public class EngineSpecs {
    public final Belief prior;
    public final Recommender recommender;
    public final QueryFilter filter;
    public final QuerySelector selector;
    public final int numberOfQueries;

    public EngineSpecs(Belief prior,
                       Recommender recommender,
                       QueryFilter filter,
                       QuerySelector selector,
                       int numberOfQueries) {
        this.prior = prior;
        this.recommender = recommender;
        this.filter = filter;
        this.selector = selector;
        this.numberOfQueries = numberOfQueries;
    }
}
