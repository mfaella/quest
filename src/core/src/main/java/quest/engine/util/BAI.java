package quest.engine.util;

import bai.finder.*;
import bai.sampler.*;
import bai.core.*;
import quest.engine.model.Belief;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class BAI {
    private static maxFinderEnumeration maxFinderEnum = null;

    public enum maxFinderEnumeration {
        SmartMaxFinder, SimpleMaxFinder, SRMaxFinder, SHMaxFinder
    }

    public static void setMaxFinderEnumeration(maxFinderEnumeration finder) {
        maxFinderEnum = finder;
    }

    /** Wrapper method returning the best BAI algorithm.
     * @param maxSamples the budget in terms of the average number of samples <i>per function</i>
     *
     */
    public static MaxFinder getMaxFinder(Belief belief, List<Function> functions, int maxSamples) {
        final double sdMultiplier = 2;
        List<Sampler> samplers = new ArrayList<>(Collections.nCopies(functions.size(), belief.sampler()));
        // Note that we're not using SingleDensitySmartMaxFinder
        // because that's not the latest version of VBR

        return getMaxFinderAlgorithm(samplers, functions, samplers.size() * maxSamples, sdMultiplier);
    }

    public static MaxFinder getMaxFinderAlgorithm (List<Sampler> samplers, List<Function> functions, int budget,
                                                   double sdMultiplier) {
        MaxFinder maxFinder = null;
        switch(maxFinderEnum){
            case SimpleMaxFinder:
                maxFinder = new SimpleMaxFinder(samplers, functions, budget);
                break;
            case SmartMaxFinder:
                maxFinder = SmartMaxFinder.newFixedBudgetMCMC(samplers, functions, budget, sdMultiplier);
                break;
            case SRMaxFinder:
                maxFinder = new SRMaxFinder(samplers, functions, budget);
                break;
            case SHMaxFinder:
                maxFinder = new SHMaxFinder(samplers, functions, budget);
                break;
        }

        return maxFinder;
    }
}
