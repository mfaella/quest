package quest.engine.util;

import quest.engine.model.Domain;
import quest.engine.model.Item;
import quest.engine.model.PairwiseQuery;
import quest.engine.model.Query;

import java.util.*;

/** A set of queries.
 *  <p>
 *  This interface provides two factory methods for two use cases:
 *  <ul>
 *      <li><code>newFull</code>: a QuerySet that starts with all pairwise queries involving
 *      the items in the domain. This set can shrink (with <code>remove</code>), but does not
 *      support insertion (<code>add</code>).</li>
 *      <li><code>newEmpty</code>: a QuerySet that starts empty. This set supports insertion
 *      (<code>add</code>) and deletion (<code>remove</code>),
 *      but deletion is inefficient (linear time).</li>
 *  </ul>
 *
 *  The objects returned by the two factory methods are <i>not</i> thread-safe.
 */
public interface QuerySet {
    /** Returns a new QuerySet that starts with all pairwise queries involving
     *  the items in the domain. This set can shrink (with <code>remove</code>), but does not
     *  support insertion (<code>add</code>).
     */
    static QuerySet newFull(Domain dom) {
        return new DecreasingLazyPairwiseQuerySet(dom);
    }

    /** Returns a new QuerySet that starts empty. This set supports insertion
     * (<code>add</code>) and deletion (<code>remove</code>), but deletion is inefficient (linear time).
     * */
    static QuerySet newEmpty() {
        return new IncreasingQuerySet();
    }

    /** Returns a random query from this set. */
    Query getRandom();

    /** Adds a query to this set (optional operation)
     * @return true if the query did not belong to the set prior to insertion, false otherwise
     * @throws UnsupportedOperationException if insertion is not supported by this implementation
     */
    boolean add(Query q) throws UnsupportedOperationException;

    /** Removes a query from this set
     * @param q a query
     * @return true if the query belonged to the set prior to removal, false otherwise
     */
    boolean remove(Query q);

    /** Checks membership in this set
     * @param q a query
     * @return true if the query belongs to the set, false otherwise
     */
    boolean contains(Query q);

    /** Returns an unmodifiable list representing the current state of this set.
     * If this set is modified with <code>add</code> or <code>remove</code>
     * after having called this method, the returned list will not reflect the changes.
     *
     * If this method is called on an initially-full set, it may require linear time.
     *
     * <p>The returned list supports constant-time positional access with <code>get</code>
     * @return an unmodifiable list representing the current state of this set
     */
    List<Query> toUnmodifiableList();

    int size();
    QuerySet clone();
}

/** This QuerySet starts empty and can grow and shrink.
 *  However, a single removal takes linear worst-case time.
 *  This class is <i>not</i> thread-safe.
 */
class IncreasingQuerySet implements QuerySet, Cloneable {
    private final Set<Query> set;
    private final List<Query> list;

    private static final Random rand = new Random();

    IncreasingQuerySet(Set<Query> set, List<Query> list) {
        this.set = set;
        this.list = list;
    }

    IncreasingQuerySet() {
        this(new HashSet<>(), new ArrayList<>());
    }

    @Override
    public List<Query> toUnmodifiableList() {
        return Collections.unmodifiableList(list);
    }

    @Override
    public boolean add(Query q) {
        if (!set.add(q)) return false;
        list.add(q);
        return true;
    }

    @Override
    public boolean contains(Query q) {
        return set.contains(q);
    }

    @Override
    public Query getRandom() {
        return list.get(rand.nextInt(size()));
    }

    @Override
    public boolean remove(Query q) {
        return set.remove(q) && list.remove(q);
    }

    @Override
    public int size() {
        return set.size();
    }

    /** Cloning is used to make copying available at the level of the interface QuerySet.
      * It does not call super.clone() because of the final fields. */
    @Override
    public IncreasingQuerySet clone() {
        Set<Query> set = new HashSet<>(this.set);
        List<Query> list = new ArrayList<>(this.list);
        return new IncreasingQuerySet(set, list);
    }
}

/** This QuerySet starts full and cannot grow (<code>add</code> is unsupported).
 *  This class is <i>not</i> thread-safe.
 */
class DecreasingLazyPairwiseQuerySet implements QuerySet, Cloneable {
    private final Set<Query> removed;
    private final int sizeWhenFull;
    private final List<Item> items;
    private List<Query> theList; // lazily instantiated
    private static final Random rand = new Random();

    DecreasingLazyPairwiseQuerySet(Domain dom) {
        removed = new HashSet<>();
        sizeWhenFull = dom.items.size() * (dom.items.size()-1) / 2;
        items = dom.items;
    }

    /** Copy constructor */
    DecreasingLazyPairwiseQuerySet(DecreasingLazyPairwiseQuerySet other) {
        removed = new HashSet<>(other.removed);
        sizeWhenFull = other.sizeWhenFull;
        items = other.items;
        theList = other.theList;
    }

    @Override
    public boolean add(Query q) {
        throw new UnsupportedOperationException("A QuerySet that starts full does not support insertion.");
    }

    @Override
    public boolean contains(Query q) {
        return !removed.contains(q);
    }

    @Override
    public Query getRandom() {
        if (size()==0)
            throw new IllegalStateException();
        while (true) {
            int i = rand.nextInt(items.size());
            int j = rand.nextInt(items.size());
            if (i==j)
                continue;
            Query q = new PairwiseQuery(items.get(i), items.get(j));
            if (!removed.contains(q))
                return q;
        }
    }

    @Override
    public boolean remove(Query q) {
        if (removed.add(q)) {
            theList = null; // invalidate the cached list
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int size() {
        return sizeWhenFull - removed.size();
    }

    @Override
    public List<Query> toUnmodifiableList() {
        if (theList==null)
            instantiate();
        return Collections.unmodifiableList(theList);
    }

    private void instantiate() {
        if (theList!=null)
            throw new IllegalStateException();

        theList = new ArrayList<>(size());
        for (Item i : items)
            for (Item j : items) {
                if (i == j)
                    break;
                Query q = new PairwiseQuery(i, j);
                if (!removed.contains(q))
                    theList.add(q);
            }
    }

    /** Cloning is used to make copying available at the level of the interface QuerySet.
      * It does not call super.clone() because of the final fields. */
    @Override
    public DecreasingLazyPairwiseQuerySet clone() {
        return new DecreasingLazyPairwiseQuerySet(this);
    }
}