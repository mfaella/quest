package quest.engine.util;

import bai.core.Density;

import java.util.List;

public class Statistics {
    /** Given a function T -> double, that assigns a value to each element of type T, it
        returns the arithmetic mean of the values associated with the samples received in input
     */
    public static <T> double arithmeticMean(java.util.function.ToDoubleFunction<T> value,
                              List<T> samples) {
        return samples.stream().mapToDouble(value).average().orElseThrow();
    }

    /** Given a function double[] -> double, it samples the specified number of elements from
        the given density and returns the arithmetic mean of the values associated with them
     */
    public static double arithmeticMean(java.util.function.ToDoubleFunction<double[]> value,
                          Density density, int sampleCount) {
        if (sampleCount<=0)
            throw new IllegalArgumentException("Cannot estimate integral with no samples.");

        List<double[]> samples = density.sampler().sample(sampleCount);
        return arithmeticMean(value, samples);
    }
}
