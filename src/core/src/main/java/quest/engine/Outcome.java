package quest.engine;

import java.io.*;
import java.util.*;

public class Outcome implements Comparable<Outcome> { 

    private String filterTag, selectorTag, recommenderTag, query, rank;
	private int userCount;
	private int queryCount;
    private int recommendedItemIndex;
    private int topItemIndex;
    private double recommendedEval;
	private double topEval;
	private double bottomEval;
    private double elapsedTime;

	public Outcome(String filterTag,
				   String selectorTag,
				   String recommenderTag,
				   int userCount,
				   int queryCount,
				   int recommendedItemIndex,
				   int topItemIndex,
				   double recommendedEval,
				   double topEval,
				   double bottomEval,
				   double elapsedTime,
				   String query,
				   String rank) {
		this.filterTag = filterTag;
		this.selectorTag = selectorTag+"s";
		this.recommenderTag = recommenderTag+"r";
		this.userCount = userCount;
		this.queryCount = queryCount;
		this.recommendedItemIndex = recommendedItemIndex;
		this.topItemIndex = topItemIndex;
		this.recommendedEval = recommendedEval;
		this.topEval = topEval;
		this.bottomEval = bottomEval;
		this.elapsedTime = elapsedTime;
		this.query = query;
		this.rank = rank;
	}

	@Override
	public int compareTo(Outcome other) {
		if (userCount != other.userCount)
			return userCount - other.userCount;
		if (queryCount != other.queryCount)
			return queryCount - other.queryCount;
		if (filterTag.compareTo(other.filterTag) != 0)
			return filterTag.compareTo(other.filterTag);
		if (selectorTag.compareTo(other.selectorTag) != 0)
			return selectorTag.compareTo(other.selectorTag);
		return recommenderTag.compareTo(other.recommenderTag);
	}

	public static String getHeader() {
		return "User\tFilter\tBI\tRec.er\tQuery\tRec.ed\tRecUtil\tTop\tTopUtil\tBottomUtil\tTime\tPair\tRank\n";
	}

	public static String getCSVHeader() {
		return "user,filter,selector,recommender,query,recommended,recUtil,top,topUtil,bottomUtil,time,pair,rank\n";
	}

	@Override
	public String toString() {
		return String.format("%d\t%s\t%s\t%s\t%d\t%d\t%.02f\t%d\t%.02f\t%.02f\t\t%.03f\t%s\t%s",
				userCount,
				filterTag,
				selectorTag,
				recommenderTag,
				queryCount,
				recommendedItemIndex,
				recommendedEval,
				topItemIndex,
				topEval,
				bottomEval,
				elapsedTime,
				query,
				rank);
	}

	public String toCSVString() {
		return String.format(Locale.US, // otherwise the decimal point may be a comma
				"%d, %s, %s, %s, %d, %d, %.02f, %d, %.02f, %.02f, %.02f, %s, %s\n",
				userCount,
				filterTag,
				selectorTag,
				recommenderTag,
				queryCount,
				recommendedItemIndex,
				recommendedEval,
				topItemIndex,
				topEval,
				bottomEval,
				elapsedTime,
				query,
				rank);
	}

	public static void saveToFile(List<Outcome> outcomeforcsv, String filename) {
		try {
			FileWriter writer = new FileWriter(filename);

			String header = Outcome.getCSVHeader();
			writer.write(header);

			for (Outcome outcome : outcomeforcsv) {
				writer.write(outcome.toCSVString());
			}
			writer.close();
		} catch (IOException e) {
			System.out.println(e);
		}
	}
}
