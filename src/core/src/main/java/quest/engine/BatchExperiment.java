package quest.engine;

import java.util.Iterator;
import org.apache.commons.math3.distribution.*;
import quest.engine.model.AnswerModel;
import quest.engine.model.User;

public class BatchExperiment implements Iterator<Oracle> {

    private int n;
    private final int userCount;
    private final MultivariateRealDistribution distribution;
    private final AnswerModel answerModel;
    
    public BatchExperiment(MultivariateRealDistribution distribution, AnswerModel answerModel, int userCount)
    {
	    this.distribution = distribution;
	    this.userCount = userCount;
	    this.answerModel = answerModel;
    }
    
    @Override
    public boolean hasNext() {
	return n < userCount;	
    }

    @Override
    public Oracle next() {
	    // fill in with new sample of parameters
	    User user = new User(distribution.sample(), answerModel);
	    n++;
	    return user;
    }
}
