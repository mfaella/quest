package quest.engine.model;

import bai.sampler.Metropolis;
import org.apache.commons.math3.distribution.MultivariateRealDistribution;

import java.util.*;

/** This belief recomputes all samples from scratch after an update.
 */
public class CleanUpdateBelief extends CachingBelief {
    private static final int INITIAL_SAMPLE_COUNT = 10;
    private static final boolean DEBUG = false;

    public CleanUpdateBelief(MultivariateRealDistribution prior, AnswerModel answerModel) {
    	super(prior, INITIAL_SAMPLE_COUNT, answerModel);
    }

    @Override
    public void update(Query q, Reply r) throws InconsistentBelief {
		super.update(q, r);

		// Pick a witness (if any)
		Optional<double[]> survivor = samples.stream().filter(this::isValid).findAny();

		if (!survivor.isPresent()) {
			// isInconsistent = true;
			// System.out.print("@");
			// return false;
			witness = null;
		} else {
			witness = survivor.get();
		}

		// Reset the cache of samples. Throw away all old samples.
		samples = new ArrayList<>();
		users = new HashMap<>();
		sampler = new Metropolis(this);

		try {
			increaseSamples(INITIAL_SAMPLE_COUNT);
		} catch (RuntimeException e) {
			isInconsistent = true;
			throw new InconsistentBelief();
		}
    }
}
