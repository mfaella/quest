package quest.engine.model;
import java.util.*;

/** A pairwise query supporting three replies:
 *  <ol>
 *     <li>item a is preferred to b</li>
 *     <li>the two items are equivalent</li>
 *     <li>item b is preferred to a</li>
 *  </ol>
 */
public class PairwiseQuery implements Query {

    public enum PairwiseReply implements Reply {
		FIRST, SECOND, INDIFFERENT;
    }

    private final Item a, b;
    private final String message;

	public PairwiseQuery(Item a, Item b) {
		this.a = a;
		this.b = b;
		this.message = "Do you prefer " + a + " or " + b + "? (three possible answers)";
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof PairwiseQuery))
			return false;
		PairwiseQuery otherQuery = (PairwiseQuery) other;
		return (a == otherQuery.a && b == otherQuery.b) || (a == otherQuery.b && b == otherQuery.a);
	}

	@Override
	public int hashCode() {
		return a.hashCode() ^ b.hashCode();
	}

	@Override
	public String toString() {
		return message;
	}

	/**
	 * Returns the number of possible replies.
	 *
	 * @return the number of possible replies
	 */
	public int getReplyCount() {
		return 3;
	}

	@Override
	public Item getA() {
		return a;
	}

	@Override
	public Item getB() {
		return b;
	}

	public Reply[] replies() {
		return PairwiseReply.values();
	}

	public static List<Query> allPairs(List<Item> items) {
		List<Query> result = new ArrayList<Query>();

		for (Item i : items)
			for (Item j : items) {
				if (i == j)
					break;
				result.add(new PairwiseQuery(i, j));
			}

		return result;
	}
}
