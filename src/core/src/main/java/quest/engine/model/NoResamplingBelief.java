package quest.engine.model;

import bai.*;

import java.util.List;
import java.util.ArrayList;
import java.util.function.Predicate;

import org.apache.commons.math3.distribution.MultivariateRealDistribution;
import org.apache.commons.lang3.tuple.*;

/** A belief based on a standard prior, provided by the Apache math library.
    Such prior can be directly sampled.

 	This belief does not perform resampling after an update.
 	It starts with a high number of samples and then filters them at each update.
 */
public class NoResamplingBelief extends CachingBelief {
    private static final int INITIAL_SAMPLE_COUNT = 10000;

	public NoResamplingBelief(MultivariateRealDistribution prior, AnswerModel answerModel) {
		super(prior, INITIAL_SAMPLE_COUNT, answerModel);
	}

	@Override
	public void update(Query q, Reply r) throws InconsistentBelief {
		super.update(q, r);
		samples.removeIf(((Predicate<double[]>) this::isValid).negate());
		isInconsistent = samples.isEmpty();
		if (isInconsistent) {
			witness = null;
			throw new InconsistentBelief();
		} else {
			witness = samples.get(0);
		}
	}
}
