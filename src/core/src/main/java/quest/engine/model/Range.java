package quest.engine.model;

/** An attribute whose possible levels are all numbers in a given range,
    with an associated unit ("meters", "dollars", etc.).
  */
public interface Range {
    double getMin();
    double getMax();
}
