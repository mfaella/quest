package quest.engine.model;

import bai.sampler.*;

import java.util.*;

import org.apache.commons.math3.distribution.MultivariateRealDistribution;

/** A belief based on a standard prior, provided by the Apache math library.
    Such prior can be directly sampled.

	Additionally, this belief maintains a cache of sample users.
 */
public abstract class CachingBelief extends Belief {
	private final MultivariateRealDistribution prior;

	/** The low-level cache **/
    protected List<double[]> samples;
	/** The mapping from low-level samples to high-level users **/
    protected Map<double[],User> users = new HashMap<>();

    protected Sampler sampler;
    protected double[] witness;

    private static final int DEFAULT_INITIAL_SAMPLE_COUNT = 100;
    private static final boolean DEBUG = false;

	public CachingBelief(MultivariateRealDistribution prior, int initialSampleCount, AnswerModel answerModel) {
		super(prior.getDimension(), answerModel);
		this.prior = prior;
		samples = new ArrayList<>(initialSampleCount);
		increaseSamples(initialSampleCount);
	}

    public CachingBelief(MultivariateRealDistribution prior, AnswerModel answerModel) {
		this(prior, DEFAULT_INITIAL_SAMPLE_COUNT, answerModel);
    }

    /** Belief is cloneable, so we need to deep copy some fields. */
	@Override
	public Belief clone() {
		CachingBelief other = (CachingBelief) super.clone();
		other.samples = new ArrayList<>(samples);
		other.users = new HashMap<>(users);
		other.sampler = null;
		return other;
    }

	@Override
	public User getUser(double[] sample) {
    	return users.get(sample);
	}

	/** Returns a sample from this belief.
	 * Used by Metropolis to start looking for samples. */
    @Override
    public double[] witness() {
		if (witness==null) {
	    	//System.out.println("No actual witness found."); // DEBUG
	    	return super.witness();
		} else {
			return witness;
		}
    }

    /* Tries to increase the sample cache to minimumSampleCount.
       If it fails to create the required number of samples, it throws RuntimeException
     */
    protected void increaseSamples(int minimumSampleCount) {
		final int missingCount = minimumSampleCount - samples.size();

		if (missingCount <= 0)
			return;

		List<double[]> newSamples;

		if (observations.isEmpty()) { // Direct sampling
			double[][] additionalSamples = prior.sample(missingCount);
			newSamples = Arrays.asList(additionalSamples);
		} else { // MCMC sampling
			if (sampler==null) {
				sampler = new Metropolis(this);
			}
			// Warning: throws unchecked exception if it can't find samples
			newSamples = sampler.sample(missingCount);
			// sanityCheck();
		}
		for (double[] sample: newSamples) {
			samples.add(sample);
			users.put(sample, new User(sample, answerModel));
		}
    }

    // Auxiliary debug method
    protected int sampleCount() {
		return samples.size();
    }

    @Override
    public final double priorEval(double[] params){
        return prior.density(params);
    }

    @Override
	public Sampler sampler() {
		return new Sampler() {
			int next; // index of the next sample to be returned

			@Override
			public double[] sample() {
				if (isInconsistent)
					return null;

				if (next >= samples.size()) {
					try {
						increaseSamples(2 * samples.size() + 1);
					} catch (RuntimeException e) {
						isInconsistent = true;
						return null;
					}
				}
				return samples.get(next++);
			}

			@Override
			public List<double[]> sample(int sampleCount) {
				if (isInconsistent)
					return Collections.emptyList();

				while (next + sampleCount > samples.size()) {
					int increase = Math.max(next + sampleCount, 2 * samples.size());
					try {
						increaseSamples(increase);
					} catch (RuntimeException e) {
						isInconsistent = true;
						return Collections.emptyList();
					}
				}
				next += sampleCount;
				return samples.subList(next-sampleCount, next);
			}
		};
	}

    private void sanityCheck() {
		int invalid = 0;
		for (double[] sample: samples) {
		    if (!isValid(sample)) {
				System.out.println(Arrays.toString(sample));
				invalid++;
	    	}
		}
		if (invalid>0)
		    System.out.println(invalid + " invalid samples out of " + samples.size());
    }
}
