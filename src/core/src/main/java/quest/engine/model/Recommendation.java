package quest.engine.model;

public class Recommendation {

    private final Item item;
    private final int index;
    private final double value;

    public Recommendation(Item item, int index, double value) {
        this.item = item;
        this.index = index;
        this.value = value;
    }

    public Item getItem() {
        return item;
    }

    public int getIndex() {
        return index;
    }

    public double getValue() {
        return value;
    }
}
