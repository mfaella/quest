package quest.engine.model;

import quest.engine.attribute.Attribute;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * The domain of an elicitation problem, consisting in:
 * - A value model (list of attributes)
 * - A list of items
 * - A list of possible queries (for example, all pairwise comparisons of items)
 *
 * Warning: This class cannot be a singleton because QuestWeb uses more than one instance
 */
public class Domain {
    public final List<Attribute> attributes;
    public final List<Item> items;
    public AnswerModel answerModel;

    // Derived info
    public final int dim;
    // Parameter bounds
    public final double[] lowerBounds, upperBounds;

    public Domain(List<Attribute> attributes, List<Item> items) {
        this.attributes = Collections.unmodifiableList(attributes);
        this.items      = Collections.unmodifiableList(items);

        dim = Attribute.computeDimension(attributes);
        /* Warning: for performance reasons this is a static setting.
           So, it doesn't make sense to have more than one list of attributes around.
           In other words, it makes little sense to have more than one Domain object. */
        // User.setupEncoding(attributes);

        lowerBounds = new double[dim]; // all zeros
        upperBounds = new double[dim]; // all ones
        Arrays.fill(upperBounds, 1);
    }
}
