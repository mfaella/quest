package quest.engine.model;

import java.util.*;
import bai.core.Density;
import org.apache.commons.lang3.tuple.*;

/** A density that can be cloned and 
    updated with a query-reply observation.

    Concrete subclasses must implement priorEval and are encouraged to override (extend)
    update and clone.
 */
public abstract class Belief extends Density implements Cloneable {
    protected List<Pair<Query, Reply>> observations = new ArrayList<>();
    protected AnswerModel answerModel;
    protected boolean isInconsistent;

    public Belief(int dim, AnswerModel answerModel) {
        super(new double[dim], upper(dim));
        this.answerModel = answerModel;
    }

    /* Abstract methods */
    /**
     * The prior that this belief is based on.
     * The (posterior) belief is obtained by evaluation the prior and
     * filtering it based on the observations.
     * In particular, the posterior is *not* normalized, for efficiency.
     */
    protected abstract double priorEval(double[] params);

    /** Returns the user associated to a given parameter vector (sample).
     *  The sample must come from a sampler obtained from this belief,
     *  otherwise null is returned. */
    public abstract User getUser(double[] sample);

    /* Concrete methods */

    public final boolean isInconsistent() { return isInconsistent; }

    /**
     * Updates this belief with a new observation.
     *
     * @throws InconsistentBelief if the observation
     * is (approximately) inconsistent with the current belief.
     * If this method launches this exception, this belief is permanently invalid.
     */
    public void update(Query q, Reply r) throws InconsistentBelief {
        observations.add(new ImmutablePair<Query, Reply>(q, r));
    }

    /** Sets the minimum number of samples that this belief should hold.
     *  This is used in the update method
     * @param minimumSampleCount
     */
    public final void setMinimumSampleCount(int minimumSampleCount) {

    }

    protected final boolean isValid(double[] sample) {
        return eval(sample) > 0;
    }

    @Override
    public Belief clone() {
        try {
            Belief copy = (Belief) super.clone();
            copy.observations = new ArrayList<>(observations);
            return copy;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError("Impossible error while cloning.");
        }
    }

    /**
     * Makes an independent copy of this belief,
     * and adds a new observation to the copy.
     * This is intended to facilitate speculation about possible futures,
     * without interfering with the current actual belief.
     */
    public Belief cloneAndUpdate(Query q, Reply r) throws InconsistentBelief {
        Belief copy = this.clone();
        copy.update(q, r);
        return copy;
    }

    /**
     * Evaluates the probability of a given user,
     * represented by its parameter vector.
     */
    @Override
    public double eval(double[] params) {
        User u = new User(params, answerModel);
        double prob = 1;
        for (Pair<Query, Reply> obs : observations) {
            Query q = obs.getKey();
            Reply r = obs.getValue();
            prob *= u.replyProbability(q, r);
        }
        if (prob>0) // Optimization: in some answer models probs are often zero
            prob *= priorEval(params);
        return prob;
    }

    /* Static methods */
    private static double[] upper(int dim) {
        double[] upper = new double[dim];
        Arrays.fill(upper, 1);
        return upper;
    }
}


