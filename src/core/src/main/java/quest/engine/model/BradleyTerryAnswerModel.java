package quest.engine.model;

import org.apache.commons.math3.distribution.EnumeratedDistribution;
import org.apache.commons.math3.util.Pair;

import java.util.ArrayList;
import java.util.List;

/** In the BradleyTerryAnswerModel the probability of reply is modeled as a
    softmax with temperature TAU of the items' utilities. The temperature determines
    how confident the user is in its replies. If TAU < 1 the probability of reply
    will shift more towards the item with higher utility; if TAU > 1 it will be
    more balanced between the two items.
    The THETA parameter determines the probability that the user is indifferent
    between the two items.
 */
public class BradleyTerryAnswerModel implements AnswerModel{
    private final double THETA;
    private final double TAU;

    public BradleyTerryAnswerModel(double theta, double tau) {
        if(theta < 1 || tau <= 0)
            throw new IllegalArgumentException();
        THETA = theta;
        TAU = tau;
    }

    @Override
    public Reply answer(Query q, User u) {
        List<Pair<Reply, Double>> pmf = new ArrayList<>();
        for(Reply reply: q.replies())
            pmf.add(new Pair<Reply, Double>(reply,replyProbability(q, u, reply)));

        EnumeratedDistribution<Reply> distribution = new EnumeratedDistribution<>(pmf);
        return distribution.sample();
    }

    @Override
    public double replyProbability(Query q, User u, Reply r) {
        PairwiseQuery.PairwiseReply reply = (PairwiseQuery.PairwiseReply) r;

        double expA = Math.exp(u.eval(q.getA())/TAU);
        double expB = Math.exp(u.eval(q.getB())/TAU);
        double probability = 0;

        switch (reply) {
            case FIRST:
                probability = expA/(expA + THETA*expB);
                break;
            case SECOND:
                probability = expB/(THETA*expA + expB);
                break;
            case INDIFFERENT:
                probability = (expA*expB*(THETA*THETA - 1))/((expA + THETA*expB)*(THETA*expA + expB));
                break;
        }

        return probability;
    }

    @Override
    public double[] replyProbabilityGradient(Query q, User u, Reply r) {
        PairwiseQuery.PairwiseReply reply = (PairwiseQuery.PairwiseReply) r;

        double[] gradientA = u.evalGradient(q.getA());
        double[] gradientB = u.evalGradient(q.getB());
        double[] probabilityGradient = new double[2*gradientA.length];
        double expA = Math.exp(u.eval(q.getA())/TAU);
        double expB = Math.exp(u.eval(q.getB())/TAU);
        double multiplier = 0;

        switch (reply) {
            case FIRST:
                multiplier = (THETA*expA*expB)/(TAU*Math.pow(expA + THETA*expB, 2));
                break;
            case SECOND:
                multiplier = -(THETA*expA*expB)/(TAU*Math.pow(THETA*expA + expB, 2));
                break;
            case INDIFFERENT:
                multiplier = (THETA*(THETA*THETA - 1)*(expA*Math.pow(expB, 3) - Math.pow(expA, 3)*expB)) /
                        (TAU*Math.pow(expA + THETA*expB, 2)*Math.pow(THETA*expA + expB, 2));
                break;
        }

        int j = 0;
        for(int i = 0; i < gradientA.length; i++) {
            probabilityGradient[j] = gradientA[i] * multiplier;
            j++;
        }

        for(int i = 0; i < gradientB.length; i++) {
            probabilityGradient[j] = gradientB[i] * (-multiplier);
            j++;
        }

        return probabilityGradient;
    }
}
