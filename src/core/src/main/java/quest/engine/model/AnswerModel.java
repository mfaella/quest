package quest.engine.model;

public interface AnswerModel {
    /** Returns the reply that User u would give to Query q according
        to the answer model
     */
    Reply answer(Query q, User u);

    /** Given a Query q, a User u  and a Reply r, returns the probability that
        u would answer with r to q, which can be written as R_q,u(r).
        This is the key method of an AnswerModel
     */
    double replyProbability(Query q, User u, Reply r);

    /** Returns the gradient with respect to the Query q of the reply probability
        function (R_q,u(r)), given a User and a Reply.
        This method is intended only for those answer models that support gradient-based
        optimization techniques for EBI. If an answer model does not support gradient calculation,
        this method should throw an UnsupportedOperationException
     */
    double[] replyProbabilityGradient(Query q, User u, Reply r);
}
