package quest.engine.model;

public interface Query {
    Item getA();
    Item getB();

    Reply[] replies();
}
