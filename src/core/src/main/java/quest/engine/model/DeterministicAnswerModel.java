package quest.engine.model;

/** In the DeterministicAnswerModel the user always answers with the item
    maximizing its utility, unless the items' utilities are too "close", in
    which case the user is indifferent.
    The DELTA parameter here is called indecision gap and is used additively
    to determine when the utilities are too close.
    This model doesn't support gradient calculation.
 */
public class DeterministicAnswerModel implements AnswerModel{
    private final double DELTA;

    public DeterministicAnswerModel(double delta) {
        if(delta < 0)
            throw new IllegalArgumentException();
        DELTA = delta;
    }

    @Override
    public Reply answer(Query q, User u) {
        double valA = u.eval(q.getA());
        double valB = u.eval(q.getB());

        if(valA - valB > DELTA)
            return PairwiseQuery.PairwiseReply.FIRST;
        else if(valA - valB < -DELTA)
            return PairwiseQuery.PairwiseReply.SECOND;
        else
            return PairwiseQuery.PairwiseReply.INDIFFERENT;
    }

    @Override
    public double replyProbability(Query q, User u, Reply r) {
        PairwiseQuery.PairwiseReply reply = (PairwiseQuery.PairwiseReply) r;

        double valA = u.eval(q.getA());
        double valB = u.eval(q.getB());
        double probability = 0;

        switch (reply) {
            case FIRST:
                probability = (valA - valB > DELTA) ? 1 : 0;
                break;
            case SECOND:
                probability = (valA - valB < -DELTA) ? 1 : 0;
                break;
            case INDIFFERENT:
                probability = (Math.abs(valA - valB) <= DELTA) ? 1 : 0;
                break;
        }
        return probability;
    }

    @Override
    public double[] replyProbabilityGradient(Query q, User u, Reply r) {
        throw new UnsupportedOperationException("This answer model does not support probability gradient calculation");
    }
}
