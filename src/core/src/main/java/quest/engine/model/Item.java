package quest.engine.model;
import quest.engine.attribute.Attribute;

import java.util.*;

public class Item {
    private String name;

    // [0,1]-Normalized levels of the active attributes.
    // The level of the i-th active attribute in the activeAttributes list
    // is stored in the i-th cell of this array
    private final double[] levels;
    private final List<Attribute> attributes;       // All attributes
    private final List<Attribute> activeAttributes; // Only active attributes
    private final Map<String,Object> extraLevels;   // Only not active attributes values
    
    public Item(String name, List<Attribute> attributes,List<Attribute> activeAttributes) {
        this.activeAttributes = activeAttributes;
	    this.attributes = attributes;
	    this.levels = new double[activeAttributes.size()];
	    this.name = name;
	    this.extraLevels = new HashMap<String,Object>();
    }
    
    public Item setAttributeLevel(double val, int index){
        levels[index] = val;
	    return this;
    }

    public void addExtraAttribute(String attributeName,Object attribute){
        extraLevels.put(attributeName,attribute);
    }

    public String getName() { return name; }

    //Used by Quest-web
    public String getUserFacingAttributeValue(Attribute attr) {
        return attr.toString(levels[attr.getIndex()]);
    }

    public Map<String, Object> getExtraLevels(){
        return extraLevels;
    }

    public double[] getAttributeLevels(){
        return levels;
    }

    //return all the attributes
    public List<Attribute> getAttributes(){
        return attributes;
    }

    public List<Attribute> getActiveAttributes() {
        return activeAttributes;
    }

    // public double getAttributeLevel(Attribute a) {...}

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        if (name != null)
            buffer.append(name);
        buffer.append("[");
        int i = 0;
        for (Attribute attrib : activeAttributes) {
            buffer.append(attrib.getName() + ": " + attrib.toString(levels[i++]));
            buffer.append(", ");
        }
        buffer.append("]");
        return buffer.toString();
    }
}
