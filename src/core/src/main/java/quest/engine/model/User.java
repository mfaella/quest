package quest.engine.model;
import quest.engine.Oracle;
import quest.engine.attribute.Attribute;

import java.util.*;

/** Warning: this class is not thread-safe. */
public class User implements Oracle {

    /* Parameters are divided into /weights/ and /shape parameters/.
       params format:
          w_0 w_1 ... w_n p_0_0 ... p_0_k0 p_1_0 ... p_1_k1 ... p_n_0 ... p_n_kn
       
       where:
          w_i is the /weight/ for the i-th active attribute
	      n+1 is the number of active attributes
          p_0_0 ... p_0_k0 are the /shape parameters/ for the first active attribute
	      etc.

       All parameters are assumed to lie in [0,1].

       We initially tolerate weights whose sum is greater than 1,
       but we normalize them the first time eval is called (lazy normalization).
       To do: check that this optimization is useful.
     */
    private final double[] params;
    private final AnswerModel answerModel;
    private boolean normalized;
    private int topItemIndex = -1;
    private int bottomItemIndex = -1;

    public User(double[] params, AnswerModel answerModel) {
    	if (params == null || answerModel == null)
	        throw new IllegalArgumentException();
        this.params = params;
        this.answerModel = answerModel;
    }

    private synchronized void normalizeWeights(int attributeCount) {
        double totalWeight = 0;
        for (int i=0; i<attributeCount; i++) {
            totalWeight += params[i];
        }
        for (int i=0; i<attributeCount; i++) {
            params[i] /= totalWeight;
        }
        normalized = true;
    }

    private double[] getParams() {
        return Arrays.copyOf(params, params.length);
    }

    /** Returns the utility value that this user attributes to the given item.
          Warning: this method is performance-critical! */
    public double eval(Item item) {
	    List<Attribute> attributes = item.getActiveAttributes();
        int attributeCount = attributes.size();

        assert params.length == Attribute.computeDimension(item.getActiveAttributes()) : "Wrong dimension";

        // Warning: not thread safe
        // However, normalizeWeights is synchronized, so in the worst case
        // we normalize twice (or multiple times), which is idempotent
        if (!normalized)
            normalizeWeights(attributeCount);

        double result=0;
	    double[] levels = item.getAttributeLevels();
        int i=0, j=attributeCount;
	
	    // System.out.println(Arrays.toString(params));

	    for (Attribute attribute: attributes) {
	        // System.out.println(entry.getKey() + " __ " + entry.getValue());
            result += params[i] * attribute.eval(params, j, levels[i]);
	        //if (result < 0) {
		    //	System.out.println(attribute + ":" + params[i] + ", " + params[j] + ", " + levels[i] + ", " + result);
	        //}
	        i++;
	        j += attribute.paramCount();
        }
        return result;
    }

    /** Returns the gradient of the utility function of this user with respect to
       the levels of the attributes of the given item
     */
    public double[] evalGradient(Item item) {
        List<Attribute> attributes = item.getActiveAttributes();
        int attributeCount = attributes.size();

        assert params.length == Attribute.computeDimension(item.getActiveAttributes()) : "Wrong dimension";

        // Warning: not thread safe
        // However, normalizeWeights is synchronized, so in the worst case
        // we normalize twice (or multiple times), which is idempotent
        if (!normalized)
            normalizeWeights(attributeCount);

        double[] gradient = new double[attributeCount];
        double[] levels = item.getAttributeLevels();
        int i=0, j=attributeCount;

        for(Attribute attribute: attributes) {
            // The partial derivative of the utility function with respect to the item's
            // i-th level is equal to the weight of the i-th attribute multiplied by its
            // derivative in that point
            gradient[i] = params[i] * attribute.evalDerivative(params, j, levels[i]);
            i++;
            j += attribute.paramCount();
        }

        return gradient;
    }

    /** Overriding Oracle and wrapping AnswerModel methods*/
    @Override
    public Reply answer(Query q) {
	    return answerModel.answer(q, this);
    }

    public double replyProbability(Query q, Reply r) {
        return answerModel.replyProbability(q, this, r);
    }

    public double[] replyProbabilityGradient(Query q, Reply r) {
        return answerModel.replyProbabilityGradient(q, this, r);
    }

    public double getTopItemValue(List<Item> items) {
        return eval(getTopItem(items));
    }

    public double getBottomItemValue(List<Item> items) {
        return eval(getBottomItem(items));
    }
    
    public int getTopItemIndex(List<Item> items) {
	    if (topItemIndex >= 0) {
    	    return topItemIndex;
    	}

	    // System.out.print("u");

    	double max_value = Double.NEGATIVE_INFINITY;
    	int i = 0, topIndex = 0;
        for (Item item: items) {
            double val = eval(item);
            if (val>max_value) {
                max_value = val;
	    	    topIndex = i;
	        }
	        i++;
        }
	    this.topItemIndex = topIndex;
        return topIndex;	
    }

    public int getBottomItemIndex(List<Item> items) {
        if (bottomItemIndex >= 0) {
            return bottomItemIndex;
        }

        // System.out.print("u");

        double min_value = Double.POSITIVE_INFINITY;
        int i = 0, bottomIndex = 0;
        for (Item item: items) {
            double val = eval(item);
            if (val<min_value) {
                min_value = val;
                bottomIndex = i;
            }
            i++;
        }
        this.bottomItemIndex = bottomIndex;
        return bottomIndex;
    }
    
    public Item getTopItem(List<Item> items){
	    return items.get(getTopItemIndex(items));
    }

    public Item getBottomItem(List<Item> items){
        return items.get(getBottomItemIndex(items));
    }

    @Override
    public String toString() {
	    return this.hashCode() + ":" + Arrays.toString(params);
    }
    
    // Static stuff

    /** Returns the maximum utility that can be reported by the eval method. */
    public static double getMaxUtility() {
	    return 1;
    }
}
