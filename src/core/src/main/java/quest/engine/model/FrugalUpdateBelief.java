package quest.engine.model;

import bai.sampler.Metropolis;
import bai.sampler.Sampler;
import org.apache.commons.math3.distribution.MultivariateRealDistribution;

import java.util.*;

/** Upon update, this belief keeps the old samples that are still valid,
 * 	and then adds more if necessary.
 *
 * 	WARNING: This should only be used with a deterministic answer model, where
 * 	the only effect of a new observation is to invalidate some samples.
 * 	With general answer models this updating policy doesn't make sense.
 */
public class FrugalUpdateBelief extends CachingBelief {
    private static final int INITIAL_SAMPLE_COUNT = 10;
    private static final boolean DEBUG = false;

    public FrugalUpdateBelief(MultivariateRealDistribution prior, AnswerModel answerModel) {
		super(prior, INITIAL_SAMPLE_COUNT, answerModel);
    }

    @Override
    public void update(Query q, Reply r) throws InconsistentBelief {
		super.update(q, r);

		// Filter old samples
		double percentage = samples.size(); // DEBUG

		Iterator<double[]> i = samples.iterator();
		while (i.hasNext()) {
			double[] sample = i.next();
			if (!isValid(sample)) {
				i.remove();
				users.remove(sample);
			}
		}

		if (DEBUG) {
			percentage = (samples.size() / percentage) * 100;
			System.out.print(" " + percentage + "% ");
		}

		// Pick a witness (if any)
		if (samples.isEmpty()) {
			witness = null;
		} else {
			witness = samples.get(0);
		}
		sampler = new Metropolis(this);
		try {
			increaseSamples(INITIAL_SAMPLE_COUNT);
		} catch (RuntimeException e) {
			isInconsistent = true;
			throw new InconsistentBelief();
		}
    }
}
