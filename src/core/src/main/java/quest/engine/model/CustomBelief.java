package quest.engine.model;

import bai.core.*;
import bai.sampler.*;

/** A belief based on a general prior.
    To sample from this belief, a Metropolis MCMC technique is applied.
 */
public class CustomBelief extends Belief {
    private final Density prior;
    
    public CustomBelief(Density prior, AnswerModel answerModel) {
	    super(prior.dim(), answerModel);
	    this.prior = prior;
    }

    @Override
    public double priorEval(double[] params){
        return prior.eval(params);
    }
    
    @Override
    public Sampler sampler() {
	return new Metropolis(this);
    }

    @Override
    public User getUser(double[] sample) {
        return null;
    }
}
