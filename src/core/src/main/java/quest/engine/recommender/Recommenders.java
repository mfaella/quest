package quest.engine.recommender;

import quest.engine.model.Domain;
import quest.engine.util.BAI;

public interface Recommenders {
    public static Recommender getMaxValue(Domain domain, int maxSamples) {
        return new MaxValue(domain, maxSamples);
    }
    public static Recommender getAbsoluteRegret(Domain domain, int maxSamples) {
        return new MinRegret(domain, maxSamples, true);
    }
    public static Recommender getRelativeRegret(Domain domain, int maxSamples) {
        return new MinRegret(domain, maxSamples, false);
    }
    public static Recommender getMaxProbability(Domain domain, int maxSamples) {
        return new MaxProbability(domain, maxSamples);
    }
    public static Recommender getMaxRelValue(Domain domain, int maxSamples) {
        return new MaxRelValue(domain, maxSamples);
    }
}
