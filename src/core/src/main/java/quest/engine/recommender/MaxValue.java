package quest.engine.recommender;

import quest.engine.model.*;
import bai.core.*;
import bai.finder.*;
import quest.engine.util.BAI;

import java.util.*;

/** The recommender returning the top item based on maximum expected value.
 */
public class MaxValue extends AbstractRecommender {
	public MaxValue(Domain domain, int maxSamples) {
    	super(domain, maxSamples);
    }

	@Override
	public Recommendation getRecommendation(Belief belief) {

		// Prepare inputs
		List<Function> functions = new ArrayList<>();
		for (Item item : items) {
			Function.Map f = (double[] params) -> {
				User u = belief.getUser(params);
				return u.eval(item);
			};
			functions.add(Function.make(f, domain.dim));
		}
		MaxFinder vbr = BAI.getMaxFinder(belief, functions, maxSamples);
		return new Recommendation(items.get(vbr.getIndexOfMax()),vbr.getIndexOfMax(),vbr.getValueOfMax().getValue());
	}

	@Override
	protected double eval(User user, Item item) {
		return user.eval(item);
	}

	@Override
	public String getTag() { return "Val"; }
}
