package quest.engine.recommender;

import quest.engine.model.Belief;
import quest.engine.model.Domain;
import quest.engine.model.Item;
import quest.engine.model.User;
import quest.engine.util.BAI;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractRecommender implements Recommender {

    protected final Domain domain;
    protected final List<Item> items;
    protected final int maxSamples;

    public AbstractRecommender(Domain domain, int maxSamples) {
        this.domain = domain;
        this.maxSamples = maxSamples;
        this.items = domain.items;
    }

    /** The evaluation of an item by an user, according to this recommender.
     *  A higher value indicates higher preference.
     *  Used by the <code>sort</code> method.
     */
    protected abstract double eval(User user, Item item);

    @Override
    public Item getRecommendedItem(Belief belief) {
        return getRecommendation(belief).getItem();
    }

    @Override
    public int getRecommendationIndex(Belief belief) {
        return getRecommendation(belief).getIndex();
    }

    @Override
    public double getRecommendationValue(Belief belief) {
        return getRecommendation(belief).getValue();
    }

    protected static class SortResult {
        public final List<Item> sortedItems;
        public final Map<Item,Double> recVal;

        protected SortResult(List<Item> sortedItems, Map<Item, Double> recVal) {
            this.sortedItems = sortedItems;
            this.recVal = recVal;
        }
    }

    protected SortResult internalSort(Belief belief) {
        Map<Item, DoubleSummaryStatistics> valueMap = new HashMap<>();
        for (Item item: items)
            valueMap.put(item, new DoubleSummaryStatistics());

        // Important: The following line establishes the policy for the number of samples
        // when sorting
        final int userCount = maxSamples * (int) Math.ceil(Math.log(items.size()));

        belief.sampler().sample(userCount).stream().map(belief::getUser).forEach(
                user -> {
                    for (Item item: items) {
                        double value = eval(user, item);
                        valueMap.get(item).accept(value);
                    }
                }
        );
        Comparator<Item> c = Comparator.<Item>comparingDouble(a -> valueMap.get(a).getAverage()).reversed();
        List<Item> result = new ArrayList<>(items);
        result.sort(c);
        return new SortResult(result,
                valueMap.entrySet().stream().collect(
                        Collectors.toMap(Map.Entry::getKey, entry-> entry.getValue().getAverage())));
    }

    @Override
    public List<Item> sort(Belief belief) {
        return internalSort(belief).sortedItems;
    }
}
