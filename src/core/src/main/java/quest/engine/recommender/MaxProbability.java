package quest.engine.recommender;

import quest.engine.model.*;
import bai.core.*;
import bai.finder.*;
import quest.engine.util.BAI;

import java.util.*;

/** The recommender returning the item which has maximal likelihood being the top item.
 */
public class MaxProbability extends AbstractRecommender {
    private static final double tolerance = 0.01;

    public MaxProbability(Domain domain, int maxSamples) {
        super(domain, maxSamples);
    }

    @Override
    public Recommendation getRecommendation(Belief belief) {

        // Prepare inputs
        List<Function> functions = new ArrayList<>();
        for (Item item : items) {
            Function.Map f = (double[] params) -> {
                User u = belief.getUser(params);
                double topValue = u.getTopItemValue(items);
                return topValue - u.eval(item) <= tolerance ? 1 : 0;
            };
            functions.add(Function.make(f, domain.dim));
        }
        MaxFinder vbr = BAI.getMaxFinder(belief, functions, maxSamples);
        return new Recommendation(items.get(vbr.getIndexOfMax()),vbr.getIndexOfMax(),vbr.getValueOfMax().getValue());
    }

    @Override
    protected double eval(User user, Item item) {
        double topValue = user.getTopItemValue(items);
        return topValue - user.eval(item) <=  tolerance ? 1 : 0;
    }

    @Override
    public String getTag() { return "Prb"; }
}
