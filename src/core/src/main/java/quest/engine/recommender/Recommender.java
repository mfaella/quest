package quest.engine.recommender;

import quest.engine.model.Belief;
import quest.engine.model.Item;
import quest.engine.model.Recommendation;

import java.util.List;

/**
 * A policy to select the best item given a belief.
 */
public interface Recommender {
    Recommendation getRecommendation(Belief belief);

    Item getRecommendedItem(Belief belief);

    int getRecommendationIndex(Belief belief);

    double getRecommendationValue(Belief belief);

    /**
     * Sort all items from the most recommended to the least recommended.
     * @param belief The current belief
     * @return the list of all items, sorted by value
     */
    List<Item> sort(Belief belief);

    /**
     * @return a three-letter tag identifying this recommender in
     * a comparative experiment
     */
    String getTag();
}
