package quest.engine.recommender;

import quest.engine.model.*;
import bai.core.*;
import bai.finder.*;
import quest.engine.util.BAI;

import java.util.*;

/** The recommender returning the top item based on maximum expected value of alpha where
 * alpha is equal to: (u(a) - u(bottomItem)) / (u(topItem) - u(bottomItem)).
 */
public class MaxRelValue extends AbstractRecommender {

    public MaxRelValue(Domain domain, int maxSamples) {
        super(domain, maxSamples);
    }
    @Override
    public Recommendation getRecommendation(Belief belief) {

        // Prepare inputs
        List<Function> functions = new ArrayList<>();
        for (Item item : items) {
            Function.Map f = (double[] params) -> {
                User u = belief.getUser(params);
                double topValue = u.getTopItemValue(items);
                double bottomValue = u.getBottomItemValue(items);
                return ((u.eval(item) - bottomValue) / (topValue - bottomValue));
            };
            functions.add(Function.make(f, domain.dim));
        }
        MaxFinder vbr = BAI.getMaxFinder(belief, functions, maxSamples);
        return new Recommendation(items.get(vbr.getIndexOfMax()),vbr.getIndexOfMax(),vbr.getValueOfMax().getValue());
    }

    @Override
    protected double eval(User user, Item item) {
        double topValue = user.getTopItemValue(items);
        double bottomValue = user.getBottomItemValue(items);
        return ((user.eval(item) - bottomValue) / (topValue - bottomValue));
    }

    @Override
    public String getTag() { return "ReV"; }
}
