package quest.engine.recommender;

import quest.engine.model.*;
import bai.core.*;
import bai.finder.*;
import quest.engine.util.BAI;

import java.util.*;

/** The recommender based on minimum absolute or relative regret.
 */
public class MinRegret extends AbstractRecommender {

    private final boolean absolute;

    public MinRegret(Domain domain, int maxSamples, boolean isAbsoluteRegret) {
        super(domain, maxSamples);
        this.absolute = isAbsoluteRegret;
    }

    @Override
    public Recommendation getRecommendation(Belief belief) {
        SortResult result = internalSort(belief);
        Item topItem = result.sortedItems.get(0);
        int topIndex = items.indexOf(topItem);
        double topValue = result.recVal.get(topItem);
        return new Recommendation(topItem, topIndex, topValue);

        /*
        // Prepare inputs
        List<Function> functions = new ArrayList<Function>();

        for (Item item : items) {
            Function.Map f = (double[] params) -> {
                User u = belief.getUser(params);
                double topValue = u.getTopItemValue(items);
                double regret = u.eval(item) - topValue;
                return absolute ? regret : regret / topValue;
            };
            functions.add(Function.make(f, domain.dim));
        }
        MaxFinder vbr = BAI.getMaxFinder(belief, functions, maxSamples);
        return new Recommendation(items.get(vbr.getIndexOfMax()),vbr.getIndexOfMax(),vbr.getValueOfMax().getValue());
         */
    }

    @Override
    protected double eval(User user, Item item) {
        double topValue = user.getTopItemValue(items);
        double regret = user.eval(item) - topValue;
        return absolute ? regret : regret / topValue;
    }

    @Override
    public String getTag() {
        return absolute ? "AbR" : "ReR";
    }
}
