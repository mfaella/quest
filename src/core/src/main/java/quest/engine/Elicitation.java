package quest.engine;

import quest.engine.filter.TopWithAll;
import quest.engine.model.*;
import quest.engine.util.QuerySet;

import java.util.*;

/** An elicitation problem.
 *
 * Usage:
 *  1. Create an Elicitation object
 *  2. Invoke methods setRecommender, setQuerySelector, and setNumberOfQueries
 *  3. Invoke method run with an Oracle argument (the user being queried)
 *
 *  You can repeat steps 2 and 3 multiple times on the same object.
 *  You can invoke setPostQueryAction to perform a specified action after each user reply.
 */
public class Elicitation {
    private static final boolean DEBUG = false;
	
	private Domain domain;
	private Optional<PostQueryAction> postQueryAction = Optional.empty();

	public Elicitation(Domain domain) {
		this.domain = domain;
	}

	public interface PostQueryAction {
		void run(int recommendedItemIndex, int queryCount, double time, Query q, Belief belief);
	}

	/**
	 * This method can be run multiple times on the same Elicitation object.
	 * @param responder The user whose preference is going to be investigated
	 */
	public void run(EngineSpecs specs, Oracle responder, boolean viewRanksOfQuery) throws InconsistentBelief {
		Belief belief = specs.prior.clone();
		QuerySet remainingQueries = QuerySet.newFull(domain);
		int count = 0;

		Optional<Integer> previousRecommendedIndex = Optional.empty();

		/* Speed Hack:
			The TopWithAll filter needs the current recommendation (current top), either from the optional
			"previousRecommendedIndex" variable (fast) or from a query to the recommender (slow).

			When dealing with the first query for a given user, that recommendation needs to be
			computed from scratch (slow). With the following queries, the current top is already
			available because it is computed at the end of every iteration for statistical analysis reasons.
			So, it is somewhat unfair to attribute to the TopWithAll filter the computation of the top
			item only for the first query.

			The following snippet avoids this problem by computing the top item *before* the first query
			and therefore outside the timed window.
		 */

		if (specs.filter.getClass() == TopWithAll.class) {
			int priorRecommendation = specs.recommender.getRecommendationIndex(belief);
			previousRecommendedIndex = Optional.of(priorRecommendation);
			// DEBUG
			// postQueryAction.ifPresent(action -> action.run(priorRecommendation, 0, 0));
			// postQueryAction.ifPresent(action -> action.run(priorRecommendation, 0, 0));
		}

		// Main elicitation loop
		do {
			// Start of the timed window. The timed window includes filter and selector only.
			double startTime = System.currentTimeMillis();
			// Apply filter
			QuerySet filteredQueries =
					specs.filter.chooseQueries(belief, remainingQueries, specs.recommender, previousRecommendedIndex);

			// DEBUG
			// double midTime =  (System.currentTimeMillis() - startTime) / 1000.0;
			if (DEBUG)
				System.out.println(filteredQueries.size() + " queries after filter");
				// System.out.println(filteredQueries);

			// Apply selector
			Query q = specs.selector.chooseQuery(belief, filteredQueries, specs.recommender);
			assert q != null;

			// End of timed window. Computing the new recommendation is *not* part of the timing.
			double finishTime = System.currentTimeMillis();
			double elapsedTime = (finishTime - startTime) / 1000.0; // in seconds

			Reply r = responder.answer(q);
			Belief oldBelief = viewRanksOfQuery ? belief.clone() : null;
			belief.update(q, r);
			remainingQueries.remove(q);
			count++;

			// Get new recommendation
			int recommendedItemIndex = specs.recommender.getRecommendationIndex(belief);
			previousRecommendedIndex = Optional.of(recommendedItemIndex);

			// Execute the optional post-query action (used by batch experiments)
			final int finalRecommendation = recommendedItemIndex;
			final int finalCount = count;
			postQueryAction.ifPresent(action -> action.run(finalRecommendation, finalCount, elapsedTime, q, oldBelief));

			if (DEBUG)
				System.out.println("After " + count + " queries the recommendation is: " +
						specs.recommender.getRecommendation(belief));
		} while (remainingQueries.size() > 0 && count < specs.numberOfQueries);
	}

	/*
		Sets the function that will be called after each query and reply.
	 */
	public void setPostQueryAction(PostQueryAction action) { postQueryAction = Optional.of(action);	}
}
