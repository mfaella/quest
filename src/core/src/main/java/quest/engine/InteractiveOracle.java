package quest.engine;

import quest.engine.model.Query;
import quest.engine.model.Reply;

import java.util.*;

public class InteractiveOracle implements Oracle {

    private static final Scanner reader = new Scanner(System.in);

	@Override
	public Reply answer(Query q) {
		System.out.println(q);

		int n = q.replies().length;
		int reply = -1;
		do {
			System.out.println("Enter 0 to " + (n - 1) + ": ");
			while (!reader.hasNextInt()) {
				System.out.println("Enter 0 to " + (n - 1) + ": ");
				reader.next();
			}
			reply = reader.nextInt();
		} while (reply < 0 || reply > n - 1);
		return q.replies()[reply];
	}

	public static Iterator<Oracle> singleton() {
		Set<Oracle> set = Set.of(new InteractiveOracle());
		return set.iterator();
	}
}
