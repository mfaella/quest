package quest.engine.filter;

import quest.engine.model.*;
import quest.engine.recommender.Recommender;
import quest.engine.util.QuerySet;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * The filter returning all pairs including the top item (according to a recommender)
 * with any other item.
 *
 * This is a linear filter.
 */
public class TopWithAll implements QueryFilter {
    private Domain domain;
    public TopWithAll(Domain domain) {
        this.domain = domain;
    }

    /* This filter needs the current recommendation, either from the optional argument (fast)
        or through a query to the recommender (slow).
     */
    @Override
    public QuerySet chooseQueries(Belief belief, QuerySet remainingQueries,
                                  Recommender recommender,
                                  Optional<Integer> previousRecommendedIndex) {
        QuerySet result = QuerySet.newEmpty();

        // orElseGet takes a supplier, so it's lazy
        Item top = domain.items.get(previousRecommendedIndex.orElseGet(() -> recommender.getRecommendationIndex(belief)));
        for (Item other: domain.items) {
            if (other == top) continue;
            Query q = new PairwiseQuery(top, other);
            if (remainingQueries.contains(q))
                result.add(q);
        }

        return result;

    }

    @Override
    public String getTag() { return "TWA"; }
}
