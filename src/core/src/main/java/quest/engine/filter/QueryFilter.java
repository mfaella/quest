package quest.engine.filter;

import quest.engine.model.Belief;
import quest.engine.recommender.Recommender;
import quest.engine.util.QuerySet;

import java.util.Optional;

/** An object that selects a subset of queries, based on the current belief.
 *
 *  It may use the recommender or, to speed up computation,
 *  the pre-computed current recommendation.
 */
public interface QueryFilter {
    QuerySet chooseQueries(Belief belief,
                           QuerySet remainingQueries,
                           Recommender recommender,
                           Optional<Integer> previousRecommendedItemIndex);

    /**
     * @return a three-letter tag identifying this filter in
     * a comparative experiment
     */
    String getTag();
}
