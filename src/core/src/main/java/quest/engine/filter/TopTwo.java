package quest.engine.filter;

import quest.engine.model.Belief;
import quest.engine.model.Domain;
import quest.engine.model.Query;
import quest.engine.recommender.Recommender;
import quest.engine.selector.QuerySelector;
import quest.engine.util.QuerySet;

import java.util.Optional;

/**
 * Query selector that returns the query obtained by choosing the top
 * two items according to the recommender
 */
public class TopTwo extends TopN implements QueryFilter {
    public TopTwo(Domain domain) {
        super(domain, 2);
    }

    @Override
    public String getTag() {
        return "Tp2";
    }
}
