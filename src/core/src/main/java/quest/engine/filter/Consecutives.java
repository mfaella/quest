package quest.engine.filter;

import quest.engine.model.*;
import quest.engine.recommender.Recommender;
import quest.engine.util.QuerySet;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * The filter returning all pairs of consecutive items,
 * in the order induced by the current recommender.
 *
 * This is a linear filter.
 */
public class Consecutives implements QueryFilter {
    private Domain domain;
    public Consecutives(Domain domain) {
        this.domain = domain;
    }

    @Override
    public QuerySet chooseQueries(Belief belief, QuerySet remainingQueries, Recommender recommender, Optional<Integer> previousRecommendedIndex) {
        QuerySet result = QuerySet.newEmpty();
        List<Item> sortedItems = recommender.sort(belief);
        Item second = null;
        for (Item first: sortedItems) {
            if (second == null) {
                second = first;
                continue;
            }
            Query q = new PairwiseQuery(first, second);
            if (remainingQueries.contains(q)) {
                result.add(q);
            }
            second = first;
        }
        return result;
    }

    @Override
    public String getTag() { return "Con"; }
}
