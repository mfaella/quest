package quest.engine.filter;

import quest.engine.model.*;
import quest.engine.recommender.Recommender;

import java.util.ArrayList;
import java.util.List;

/**
 * The filter returning all pairs of the top
 *
 *     ceiling( square-root(n) )
 *
 * items (according to a recommender), where n is the total number of items.
 *
 * This is a linear filter.
 */
public class TopSquareRoot extends TopN {
    public TopSquareRoot(Domain domain) {
        super(domain, (int) Math.ceil(Math.sqrt(domain.items.size())));
    }

    @Override
    public String getTag() { return "SqR"; }
}
