package quest.engine.filter;

import quest.engine.model.*;
import quest.engine.recommender.Recommender;
import quest.engine.util.QuerySet;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * The abstract filter returning all pairs of the top n items (according to a recommender),
 * for a given value of n.
 * If any of those pairs are unavailable (already used) it returns fewer pairs,
 * but always at least one.
 *
 * This filter does not use the (optional) current recommendation.
 */
public abstract class TopN implements QueryFilter {
    private final Domain domain;
    private final int n;

    public TopN(Domain domain, int n) {
        this.domain = domain;
        this.n = n;
    }

    /** This implementation does not use the (optional) current recommendation.
     * @return*/
    @Override
    public QuerySet chooseQueries(Belief belief, QuerySet remainingQueries,
                                  Recommender recommender,
                                  Optional<Integer> previousRecommendedIndex) {
        return filterOrReturnSingleton(belief, remainingQueries, recommender);
    }

    public QuerySet filterOrReturnSingleton(Belief belief, QuerySet remainingQueries, Recommender recommender) {
        List<Item> sortedItems = recommender.sort(belief);
        QuerySet result = QuerySet.newEmpty();
        List<Item> topM = sortedItems.subList(0, n);
        for (Item first : topM) {
            for (Item second : topM) {
                if (first == second) break; // skip to next first
                Query q = new PairwiseQuery(first, second);
                if (remainingQueries.contains(q))
                    result.add(q);
            }
        }
        // Recover
        if (result.size() == 0) {
            for (Item first : sortedItems) {
                for (Item second : sortedItems) {
                    if (first == second) break; // skip to next first
                    Query q = new PairwiseQuery(first, second);
                    if (remainingQueries.contains(q)) {
                        result.add(q);
                        return result;
                    }
                }
            }
        }
        return result;
    }

    public QuerySet filterOrIncreaseSlate(Belief belief, QuerySet remainingQueries, Recommender recommender) {
        List<Item> sortedItems = recommender.sort(belief);
        int m = n;
        while (true) {
            QuerySet result = QuerySet.newEmpty();
            List<Item> topM = sortedItems.subList(0, m);
            for (Item first : topM) {
                for (Item second : topM) {
                    if (first == second) break; // skip to next first
                    Query q = new PairwiseQuery(first, second);
                    if (remainingQueries.contains(q))
                        result.add(q);
                }
            }
            if (result.size() > 0) {
                return result;
            }
            m++;
        }
    }
}
