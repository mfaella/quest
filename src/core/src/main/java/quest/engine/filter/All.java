package quest.engine.filter;

import quest.engine.model.*;
import quest.engine.recommender.Recommender;
import quest.engine.util.QuerySet;

import java.util.ArrayList;
import java.util.Optional;

public class All implements QueryFilter {
    private Domain domain;

    public All(Domain domain) {
        this.domain = domain;
    }

    @Override
    public QuerySet chooseQueries(Belief _belief, QuerySet remainingQueries, Recommender _recommender, Optional<Integer> _previousRecommendedIndex) {
        return remainingQueries;
    }
    @Override
    public String getTag() { return "All"; }
}
