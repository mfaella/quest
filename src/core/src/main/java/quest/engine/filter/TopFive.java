package quest.engine.filter;

import quest.engine.model.Domain;

/**
 * The filter returning all pairs of the top five items
 * (according to a recommender).
 *
 * This is a constant filter.
 */
public class TopFive extends TopN {
    public TopFive(Domain domain) {
        super(domain, 5);
    }

    @Override
    public String getTag() { return "Tp5"; }
}
