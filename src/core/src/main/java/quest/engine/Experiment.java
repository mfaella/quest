package quest.engine;

import bai.core.Function;
import bai.finder.MaxFinder;
import quest.engine.filter.QueryFilter;
import quest.engine.model.*;
import quest.engine.selector.QuerySelector;
import quest.engine.recommender.Recommender;
import quest.engine.util.BAI;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/** A batch experiment comparing different elicitation settings.
 *
 */
public class Experiment {
    private static final boolean DEBUG = false;

    private final Domain domain;
    private final Boolean statsFlag;

    // Solution technique
    private List<Recommender> recommenders = new ArrayList<>();
    private List<QuerySelector> selectors = new ArrayList<>();
    private List<QueryFilter> filters = new ArrayList<>();
    private Belief prior;
    private int numberOfQueries;
    private int numberOfThreads;
    private int numberOfUsers;
    private Iterator<Oracle> oracleModel;
    private List<Outcome> outcomes = new ArrayList<>();
    private String batchOutputName;
    private boolean viewRanksOfQuery;
    private boolean explicitRecommendersSelectors;

    private int maxSamples;

    // Parameters initialized by run()
    private Elicitation elicitation;
    private List<EngineSpecs> specs;

    public Experiment(Domain domain, Boolean statsFlag) {

        this.domain = domain;
        this.statsFlag = statsFlag;
    }

    public void setMaxSamples(int maxSamples) { this.maxSamples = maxSamples; }

    public void run() {
        System.out.println("Starting experiment");

        if (selectors == null || selectors.isEmpty())
            throw new IllegalStateException("No query selector available.");
        if (recommenders == null || recommenders.isEmpty())
            throw new IllegalStateException("No recommender available.");

        elicitation = new Elicitation(domain);
        // Create engines
        specs = new ArrayList<>();

        if(!this.explicitRecommendersSelectors) {
            // For each BeliefImprovement and recommender
            for (QueryFilter filter : filters) {
                for (QuerySelector selector : selectors) {
                    for (Recommender recommender : recommenders) {
                        EngineSpecs engine = new EngineSpecs(prior, recommender, filter, selector, numberOfQueries);
                        specs.add(engine);
                    }
                }
            }
        } else {
            int numberOfRecommendersAndSelectors = recommenders.size();
            for (QueryFilter filter : filters) {
                for (int i=0; i<numberOfRecommendersAndSelectors; i++) {
                    Recommender recommender = recommenders.get(i);
                    QuerySelector selector = selectors.get(i);
                    EngineSpecs engine = new EngineSpecs(prior, recommender, filter, selector, numberOfQueries);
                    specs.add(engine);
                }
            }
        }

        if ( !this.statsFlag ) {
            AtomicInteger userCount = new AtomicInteger();
            System.out.println(Outcome.getHeader());
            ArrayList<Thread> threadList = new ArrayList<>();

            // Create threads
            for (int i = 0; i < numberOfThreads; i++) {
                Thread t = new Thread() {
                    public void run() {
                        while (true) {
                            // Generate next user
                            Oracle responder;
                            synchronized (oracleModel) {
                                if (!oracleModel.hasNext()) break;
                                responder = oracleModel.next();
                            }
                            userCount.incrementAndGet();
                            User user = (User) responder;
                            batchExperiment(user, userCount.get());
                        }
                    }
                };
                threadList.add(t);
            }

            for (Thread t : threadList) {
                t.start();
            }
            // Aspetto che tutti i thread finiscano
            for (Thread t : threadList) {
                try {
                    t.join();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            Collections.sort(outcomes);
            Outcome.saveToFile(outcomes, batchOutputName);
        } else {
            this.showStats();
        }
    }

    // Runs the experiment on a single user and all engines
    private void batchExperiment(User user, int userCount) {

        int topItemIndex = user.getTopItemIndex(domain.items);
        double topEval = user.getTopItemValue(domain.items);
        double bottomEval = user.getBottomItemValue(domain.items);

        for (EngineSpecs engine: specs) {
            // The post-query action prints the stats
            elicitation.setPostQueryAction(
                    (recommendedItemIndex, queriesCount, elapsedTime, query, belief) -> {
                        Item recommendedItem = domain.items.get(recommendedItemIndex);
                        double recommendedEval = user.eval(recommendedItem);

                        Item itemA = query.getA();
                        Item itemB = query.getB();
                        int indexA = domain.items.indexOf(itemA);
                        int indexB = domain.items.indexOf(itemB);
                        String queryString = indexA + ":" + indexB;

                        String rank = "---";
                        if ( viewRanksOfQuery ){
                            List<Item> sortedItems = engine.recommender.sort(belief);
                            int rankA = sortedItems.indexOf(itemA);
                            int rankB = sortedItems.indexOf(itemB);
                            rank = rankA+":"+rankB;
                        }

                        Outcome outcome = new Outcome(engine.filter.getTag(), engine.selector.getTag(), engine.recommender.getTag(),
                                userCount, queriesCount,
                                recommendedItemIndex, topItemIndex,
                                recommendedEval, topEval, bottomEval, elapsedTime, queryString, rank);
                        outcomes.add(outcome);
                        System.out.println(outcome);
                    }
            );
            try {
                elicitation.run(engine, user, viewRanksOfQuery);
            } catch (InconsistentBelief exception) {
                System.err.println("Warning: inconsistency, skipping");
                continue; // Skips to next recommender
            }
        }
    }


    // Runs the experiment on a single user and all engines
    private void showStats() {
        EngineStats stats = new EngineStats(this.recommenders, this.domain, this.prior, this.maxSamples);
        System.out.println("\nPrior recommendations");
        stats.computePriorRecommendations();
        System.out.println("\nVariances");
        stats.computeVarianceOfTop();
        stats.computeVarianceOfBottom();
        System.out.println("\nDistribution of true top");
        stats.computeDistributionOfTrueTop();
    }

    public int dim() { return domain.dim; }
    public Domain getDomain() { return domain; }

    // Setters
    public void addRecommender(Recommender r) { recommenders.add(r); }
    public void addQueryFilter(QueryFilter f) { filters.add(f); }
    public void addQuerySelector(QuerySelector s) { selectors.add(s); }
    public void setBatchOutputName(String name) { batchOutputName = name; }
    public void setNumberOfQueries(int n) { numberOfQueries = n; }
    public void setNumberOfThreads(int n) { numberOfThreads = n; }
    public void setNumberOfUsers(int n) { numberOfUsers = n; }
    public void setOracleModel(Iterator<Oracle> oracles) { oracleModel = oracles; }
    public void setPrior(Belief prior) { this.prior = prior; }
    public void setViewRanksOfQuery(Boolean viewRanksOfQuery) { this.viewRanksOfQuery = viewRanksOfQuery; }
    public void setExplicitRecommendersSelectors(Boolean explicitRecommendersSelectors) { this.explicitRecommendersSelectors = explicitRecommendersSelectors; }
}
