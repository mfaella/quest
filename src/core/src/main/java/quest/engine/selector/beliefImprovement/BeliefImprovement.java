package quest.engine.selector.beliefImprovement;

import quest.engine.model.*;
import bai.core.Function;
import quest.engine.recommender.Recommender;

import java.util.HashMap;
import java.util.Map;

public interface BeliefImprovement {
    /**
     * A BI may need to invoke the current recommender.
     *
     * It returns a function that accepts a sample (a potential user)
     * and returns the updated evaluation (double) of query q.
     *
     */
    Function BIFunction(Belief belief, Query q, Recommender r);
    /**
     * @return a map containing for each reply the item recommended
     *
     */
    default Map<Reply, Item> computeRecommendations(Belief belief, Query q, Recommender recommender ) {
        Reply[] replies = q.replies();

        // Compute optimal item for each reply
        Map<Reply, Item> ev_map = new HashMap<>();
        for (Reply r : replies) {
            Belief r_updated = belief.clone();
            try {
                r_updated.update(q, r);
                Item rec = recommender.getRecommendation(r_updated).getItem();
                ev_map.put(r, rec);
            }
            catch (InconsistentBelief e) {
                // Skip this reply
            }
        }

        return ev_map;
    }
    /**
     * @return a three-letter tag identifying this BI in
     * a comparative experiment
     */
    String getTag();
}
