package quest.engine.selector;

import java.util.*;

import quest.engine.model.Belief;
import quest.engine.model.Query;
import quest.engine.selector.beliefImprovement.BeliefImprovement;
import bai.sampler.*;
import bai.core.*;
import bai.finder.*;
import quest.engine.recommender.Recommender;
import quest.engine.util.BAI;
import quest.engine.util.QuerySet;

/**
 * The query selector that wraps a BeliefImprovement object.
 */
public class Standard implements QuerySelector {

	private double sdMultiplier;
	private int maxSamples;
	private BeliefImprovement beliefImprovement;

	public static final double DEFAULT_MULTIPLIER = 2;
	private static final boolean DEBUG = false;

	/**
	 * @param beliefImprovement The belief improvement function that this selector is using.
	 * @param maxSamples Higher values increase accuracy. Reasonable values depend on the dimensionality
	 * of the elicitation problem. Usually in the range 10k-100k.
	 */
	public Standard(BeliefImprovement beliefImprovement, int maxSamples) {
		if (maxSamples <= 0)
			throw new IllegalArgumentException("Cannot work with fewer than 1 sample.");
		this.maxSamples = maxSamples;
		this.sdMultiplier = DEFAULT_MULTIPLIER;
		this.beliefImprovement = beliefImprovement;
	}

	@Override
	public Query chooseQuery(Belief belief, QuerySet candidateQueries,
							 Recommender recommender) {
		List<Query> queries = candidateQueries.toUnmodifiableList();
		if (queries.size() == 1)
			return queries.iterator().next();

		// Prepare inputs
		List<Function> functions = new ArrayList<Function>();
		for (Query q : queries) {
			functions.add(beliefImprovement.BIFunction(belief, q, recommender));
		}

		// Launch VBR
		List<Sampler> samplers = new ArrayList<>();
		for (Query q : queries) {
			samplers.add(belief.sampler());
		}
		// System.out.println(querySelector.getClass() + " maxSamples: " + maxSamples);

		MaxFinder maxFinder = BAI.getMaxFinderAlgorithm(samplers, functions, samplers.size() * maxSamples,
				sdMultiplier);
		return queries.get(maxFinder.getIndexOfMax());

		// System.out.println(" tot samples: " + ((StandardBelief)belief).sampleCount());
	
	/*if (DEBUG) {
		System.out.println("[DEBUG] All VOIs: " + smart.getAllResults());
		System.out.println("[DEBUG] Max BeliefImprovement: " + smart.getValueOfMax().getValue());
	}*/
		//return remainingQueries.get(smart.getIndexOfMax());
	}

	@Override
	public String getTag() {
		return beliefImprovement.getTag();
	}
}
