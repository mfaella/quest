package quest.engine.selector;

import java.util.*;

import quest.engine.filter.TopWithAll;
import quest.engine.model.*;
import bai.sampler.*;

import org.apache.commons.lang3.tuple.*;
import quest.engine.recommender.Recommender;
import quest.engine.util.QuerySet;
import quest.engine.util.Statistics;

/**
 * This selector identifies the query that minimizes the expected entropy
 * of the distribution of the top item.
 *
 * <h3>Dependencies with recommender and answer model</h3>
 * This selector can be used with all answer models and does not invoke the recommender.
 *
 * <h3>Performance</h3>
 * This selector iterates over all candidate queries, but it does not update
 * the belief for each possible observation (query-reply).
 *
 * <h3>Notes</h3>
 * This selector gives good results with the {@link TopWithAll} filter.
 */
public class MinTopEntropy implements QuerySelector {
    private int maxSamples;
    private Domain domain;

    // public static final int DEFAULT_MAX_SAMPLE_COUNT = 5000;
    private static final boolean DEBUG = false;

    /**
     @param domain The domain for the elicitation problem
     @param maxSamples Higher values increase accuracy. Reasonable values depend on the dimensionality
     of the elicitation problem. Usually in the range 10k-100k.
     */
    public MinTopEntropy(Domain domain, int maxSamples) {
        if (maxSamples <= 0)
            throw new IllegalArgumentException("Cannot work with fewer than 1 sample.");
        this.domain = domain;
        this.maxSamples = maxSamples;
    }

    @Override
    public Query chooseQuery(Belief belief, QuerySet candidateQueries,
                             Recommender _unused) {
        List<Query> queries = candidateQueries.toUnmodifiableList();
        if (queries.size() == 1)
            return queries.get(0);
        Query selected_query = null;
        double max_evoi = Double.NEGATIVE_INFINITY;

        List<Item> items = domain.items;

        // DEBUG
        // System.out.println("MinTopEntropy maxSamples:" + maxSamples);

        // TO DO: move this functionality to Belief
        Sampler sampler = belief.sampler();
        List<double[]> samples = sampler.sample(maxSamples *
                items.size());
        List<User> users = new ArrayList<User>();
        for (double[] sample : samples)
            users.add(belief.getUser(sample));

        for (Query q : queries) { // for each query
            Reply[] replies = q.replies();
            double query_evoi = 0;

            // Compute Pr(r_{q,u} = r), for each r
            Map<Reply, Double> probsOfReply = new HashMap<>();
            for(Reply r: replies) {
                double avgProbability = Statistics.arithmeticMean(user -> user.replyProbability(q, r), users);
                probsOfReply.put(r, avgProbability);
            }

            // DEBUG: System.out.println(probsOfReply);

            /* Compute Pr(i_u = j and r_{q,u} = r).
            Under the assumption that events i_u = j and r_{q,u} = r are independent,
            it is the arithmetic mean over the users of the product of the two event's probability.
            Since the top item of an user is deterministic, we can scan the users list
            a single time, updating only the necessary probabilities
            */
            double[][] cumulativeProb = new double[replies.length][items.size()];
            for(User u: users) {
                int top_u = u.getTopItemIndex(items);
                for(int r = 0; r < replies.length; r++) {
                    cumulativeProb[r][top_u] += u.replyProbability(q, replies[r]);
                }
            }
            // Fill a map with the computed probabilities
            Map<Pair<Reply, Item>, Double> itemReplyProb = new HashMap<>();
            for(int r = 0; r < replies.length; r++) {
                for (int i = 0; i <items.size(); i++){
                    double avgProb = cumulativeProb[r][i] / users.size();
                    itemReplyProb.put(new ImmutablePair<>(replies[r], items.get(i)), avgProb);
                }
            }

            // System.out.println("TTE:" + itemReplyProb);

            for (Reply r : replies) {
                for (Item i : items) {
                    double itemProb = itemReplyProb.get(new ImmutablePair<>(r, i));
                    double replyProb = probsOfReply.get(r);
                    query_evoi += (itemProb == 0 || replyProb == 0) ?
                            0 : itemProb * Math.log(itemProb / replyProb);
                }
            }

            // DEBUG
            // System.out.println(q + ":\t" + probsOfReply + ", " + query_evoi);

            if (query_evoi > max_evoi) {
                max_evoi = query_evoi;
                selected_query = q;
            }
        }
        return selected_query;
    }

    @Override
    public String getTag() {
        return "TTE";
    }
}
