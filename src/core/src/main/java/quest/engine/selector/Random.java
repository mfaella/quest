package quest.engine.selector;

import quest.engine.model.Belief;
import quest.engine.model.Domain;
import quest.engine.model.Query;
import quest.engine.recommender.Recommender;
import quest.engine.util.QuerySet;

public class Random implements QuerySelector {

    private static final java.util.Random random = new java.util.Random();

    // This constructor is mandatory due to reflection
    public Random(Domain domain, int sampleCount) { }

    @Override
    public Query chooseQuery(Belief _unused1, QuerySet candidateQueries,
                             Recommender _unused2) {
        return candidateQueries.getRandom();
    }

    @Override
    public String getTag() {
        return "RND";
    }
}
