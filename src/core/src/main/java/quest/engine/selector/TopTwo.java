package quest.engine.selector;

import quest.engine.filter.TopN;
import quest.engine.model.*;
import quest.engine.recommender.Recommender;
import quest.engine.util.QuerySet;

import java.util.Optional;

/**
 * Query selector that returns the query obtained by choosing the top
 * two items according to the recommender
 */
public class TopTwo extends TopN implements QuerySelector {
    public TopTwo(Domain domain, int maxSamples) {
        super(domain, 2);
    }

    @Override
    public Query chooseQuery(Belief belief, QuerySet candidateQueries,
                             Recommender recommender) {
        QuerySet singleton = chooseQueries(belief, candidateQueries, recommender, Optional.empty());
        return singleton.toUnmodifiableList().get(0);
    }

    @Override
    public String getTag() {
        return "Tp2";
    }
}
