package quest.engine.selector.beliefImprovement;

import quest.engine.model.*;
import bai.core.*;
import quest.engine.recommender.Recommender;

import java.util.*;

/**
 * This selector identifies the query that maximizes the expected relative utility of the recommendation.
 *
 * <h3>Dependencies with recommender and answer model</h3>
 * This selector can be used with all answer models and can invoke any type of recommender.
 *
 * <h3>Performance</h3>
 * This selector updates the belief and computes the recommendations for each possible observation (query-reply).
 * For each sample user and each reply, it computes the difference between the utility of the recommendation and the
 * the utility of bottom item for that user and divides this result for the difference between the utility of the top
 * item and the utility of bottom item for that user.
 *
 * <h3>Notes</h3>
 * See EBI_rval in the paper.
 */
public class MaxRelValue implements BeliefImprovement {

    private final static boolean DEBUG = false;

    private Domain domain;

    /* sampleCount is unused because this BeliefImprovement does not perform
       integration directly, but only through the recommender,
       who has its own independent settings.
    */
    public MaxRelValue(Domain domain, int sampleCount) {
        this.domain = domain;
    }
    @Override
    public Function BIFunction(Belief belief, Query q, Recommender recommender) {
        Reply[] replies = q.replies();

        Map<Reply, Item> ev_map = computeRecommendations(belief, q, recommender);

        Function.Map voi = (double[] params) -> {
            User u = belief.getUser(params);
            double topItemValue = u.getTopItemValue(domain.items);
            double bottomItemValue = u.getBottomItemValue(domain.items);
            double result = 0.0;
            double alpha;
            for(Reply r: replies) {
                double replyProb = u.replyProbability(q, r);
                if(replyProb > 0.0) { //avoid further calculations if the probability of reply is zero
                    Item rec = ev_map.get(r);
                    double itemValue = (rec != null) ? u.eval(rec) : 0; //rec can be null due to inconsistent posteriors
                    alpha = (( itemValue - bottomItemValue ) / ( topItemValue - bottomItemValue ));
                    result += replyProb * alpha;
                }
            }
            return result;
        };
        return Function.make(voi, domain.dim);
    }

    @Override
    public String getTag() {
        return "ReV";
    }
}
