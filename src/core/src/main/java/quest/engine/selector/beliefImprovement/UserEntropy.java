package quest.engine.selector.beliefImprovement;

import quest.engine.filter.TopWithAll;
import quest.engine.model.*;
import bai.core.*;
import quest.engine.recommender.Recommender;
import quest.engine.util.Statistics;

import java.util.*;

/**
 * This selector identifies the query that minimizes the expected entropy
 * of the user distribution.
 *
 * <h3>Dependencies with recommender and answer model</h3>
 * This selector can be used with all answer models and does not invoke the recommender.
 *
 * <h3>Performance</h3>
 * This selector iterates over all candidate queries, but it does not update
 * the belief for each possible observation (query-reply).
 *
 * <h3>Notes</h3>
 * See definition EVOI_hu in the paper.
 */
public class UserEntropy implements BeliefImprovement {

    private final Domain domain;
    private final int sampleCount;

    public UserEntropy(Domain domain, int sampleCount) {
    	this.domain  = domain;
		this.sampleCount = sampleCount;
    }

    @Override
    public Function BIFunction(Belief belief, Query q, Recommender _unused) {
    	Reply[] replies = q.replies();
    	// System.out.println("[DEBUG]" + q);

    	Map<Reply, Double> probabilityOfReply = new HashMap<>();
    	for(Reply r: replies) {
    		double avgProbability = Statistics.arithmeticMean(params -> belief.getUser(params).replyProbability(q, r),
					belief, sampleCount);
			probabilityOfReply.put(r, avgProbability);
		}

    	// System.out.println("UE: " + probabilityOfReply);

    	Function.Map voi = (double[] params) -> {
			User u = belief.getUser(params);
			double result = 0.0;
			for(Reply r: replies) {
				double replyProb = u.replyProbability(q, r);
				if(replyProb > 0.0) { //avoid further calculations if the probability of reply is zero
					// Note: the following call needlessly iterates over past observations,
					// while we would be perfectly fine with querying the original prior,
					// because we already know that the user "params" comes from the posterior.
					double userProb  = belief.eval(params);
					// double userProb  = ((CachingBelief)belief).priorEval(params);
					double minusEntropy = Math.log((userProb * replyProb)/probabilityOfReply.get(r));
					if(minusEntropy == Double.NEGATIVE_INFINITY || minusEntropy == Double.POSITIVE_INFINITY)
						minusEntropy = 0.0;
					result += replyProb * minusEntropy;
				}
			}
			return result;
    	};
    	return Function.make(voi, domain.dim);
    }

	@Override
	public String getTag() {
		return "UsE";
	}
}
