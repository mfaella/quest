package quest.engine.selector;

import quest.engine.filter.QueryFilter;
import quest.engine.model.Belief;
import quest.engine.model.Query;
import quest.engine.recommender.Recommender;
import quest.engine.util.QuerySet;

import java.util.Optional;

/**
   The component responsible for selecting the next query,
   based on the current belief.
 */
public interface QuerySelector extends QueryFilter {
    /**
     * Some selectors need a set of past queries, to check quickly if
     * a query has already been asked.
     * A query selector may need to invoke the current recommender.
     */
    Query chooseQuery(Belief belief, QuerySet candidateQueries, Recommender recommender);

    /**
     * By default if a selector is used as a filter, it returns a singleton
     * containing the selected query
     */
    @Override
    default QuerySet chooseQueries(Belief belief,
                                   QuerySet remainingQueries,
                                   Recommender recommender,
                                   Optional<Integer> previousRecommendedItemIndex) {
        QuerySet result = QuerySet.newEmpty();
        result.add(chooseQuery(belief, remainingQueries, recommender));
        return result;
    }
}
