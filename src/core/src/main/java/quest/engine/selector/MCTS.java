package quest.engine.selector;

import bai.sampler.Sampler;
import quest.engine.model.*;
import quest.engine.recommender.Recommender;
import quest.engine.util.QuerySet;

import java.util.HashMap;
import java.util.Map;

public class MCTS implements QuerySelector {

    private long timeout = 2000; //Computational time budget allocated to MCTS.

    private final int queriesDepth = 5; //Maximum tree depth (also reached during playouts). It must match the number of queries provided in input

    private double UCB_CONSTANT; //It balances between exploitation and exploration of the selection

    private Recommender recommender;

    // This constructor is mandatory due to reflection
    public MCTS(Domain domain, int sampleCount) {}

    @Override
    public Query chooseQuery(Belief belief, QuerySet candidateQueries, Recommender recommender) {

        UCB_CONSTANT = 1 / Math.sqrt(Math.log(candidateQueries.size()));
        this.recommender = recommender;
        DecisionNode root = new DecisionNode(null,belief,candidateQueries,null,0);
        long deadline = System.currentTimeMillis() + timeout;
        while (System.currentTimeMillis() < deadline) {
            root.mcts();
        }

        return root.mostVisitedQuery();
    }

    @Override
    public String getTag() {
        return "MCT";
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    private abstract static class MCTSNode {
        private double totalScore;
        private int visits;
        private final MCTSNode parent;

        public MCTSNode(MCTSNode parent) {
            this.parent = parent;
        }

        //Cloning constructor
        public MCTSNode(double totalScore, int visits, MCTSNode parent) {
            this.totalScore = totalScore;
            this.visits = visits;
            this.parent = parent;
        }

        @Override
        public boolean equals(Object o) {
            if (o == null)
                return false;
            if (o.getClass() != getClass())
                return false;
            MCTSNode otherNode = (MCTSNode) o;
            return Double.compare(totalScore,otherNode.totalScore) == 0 && visits == otherNode.visits && parent == otherNode.parent;
        }

        @Override
        public int hashCode() {
            return Double.hashCode(totalScore) ^ visits ^ parent.hashCode();
        }
    }

    private class DecisionNode extends MCTSNode {

        private final Belief belief;
        private final QuerySet candidateQueries;
        private final QuerySet unexpandedQueries;
        private int depth;
        private final Reply reply; //Reply the Decision Node refers to
        private final Map<Query,ResponseNode> children;

        public DecisionNode(ResponseNode parent, Belief belief, QuerySet candidateQueries, Reply reply, int depth) {
            super(parent);
            this.belief = belief;
            this.candidateQueries = candidateQueries;
            unexpandedQueries = candidateQueries.clone();
            this.depth = depth;
            this.reply = reply;
            this.children = new HashMap<>();
        }

        //Cloning constructor
        public DecisionNode(double totalScore, int visits, MCTSNode parent, Belief belief, QuerySet candidateQueries, QuerySet unexpandedQueries, int depth, Reply reply, Map<Query,ResponseNode> children) {
            super(totalScore,visits,parent);
            this.belief = belief.clone();
            this.candidateQueries = candidateQueries.clone();
            this.unexpandedQueries = unexpandedQueries.clone();
            this.depth = depth;
            this.reply = reply;
            this.children = new HashMap<>(children);
        }


        public void mcts() {
            if (isTerminal())
                backup(this);
            else if (unexpandedQueries.size() == 0) { //The nodes of the current level have all been expanded already, so the algorithm is applied recursively to the next level
                Query q = bestQuery();
                DecisionNode nextDecisionNode = sampleNext(q);
                nextDecisionNode.mcts();
            }
            else { //It is still necessary to expand all nodes of the current level
                Query q = expand();
                DecisionNode nextDecisionNode = sampleNext(q);
                DecisionNode leafNode = nextDecisionNode.playout();
                if (leafNode != null)
                    nextDecisionNode.backup(leafNode);
            }
        }

        public DecisionNode sampleNext(Query query) {
            DecisionNode result = null;
            ResponseNode responseChild = children.get(query);
            while (result == null) {
                User user = sampleUserFromBelief(belief);
                Reply reply = user.answer(query);
                result = responseChild.children.get(reply);
            }
            return result;
        }

        //It selects a query according to the Tree Policy (UCB Formula)
        public Query bestQuery() {
            double maxUCBValue = 0;
            Query result = null;
            for (Query query: children.keySet()) {
                ResponseNode responseNode = children.get(query);
                double exploitation = responseNode.getTotalScore() / responseNode.getVisits();
                double exploration = UCB_CONSTANT * Math.sqrt(Math.log(getVisits()) / responseNode.getVisits());
                double currentUCBValue = exploitation + exploration;
                //System.out.println("Exploitation: " + exploitation + ", Exploration: " + exploration); //DEBUG
                if (result == null) {
                    maxUCBValue = currentUCBValue;
                    result = query;
                }
                else {
                    if (currentUCBValue >= maxUCBValue) {
                        maxUCBValue = currentUCBValue;
                        result = query;
                    }
                }
            }
            return result;
        }

        //It creates the response node and the respective three children (decision nodes, one for each reply)
        public Query expand() {
            Query unexpandedQuery = unexpandedQueries.getRandom();
            unexpandedQueries.remove(unexpandedQuery);
            ResponseNode responseNode = new ResponseNode(unexpandedQuery,this);
            children.put(unexpandedQuery,responseNode);

            for (Reply reply: unexpandedQuery.replies()) {
                Belief updatedBelief = belief.clone();
                try {
                    updatedBelief.update(unexpandedQuery,reply);
                    QuerySet updatedCandidateQueries = candidateQueries.clone();
                    updatedCandidateQueries.remove(unexpandedQuery);
                    responseNode.children.put(reply,new DecisionNode(responseNode,updatedBelief,updatedCandidateQueries,reply,depth+1));
                } catch (InconsistentBelief ignored) {}
            }
            return unexpandedQuery;
        }

        //Queries are randomly selected until the termination condition is reached (maximum depth reached), the relative replies to the queries are provided by fictitious users sampled from the updated belief
        public DecisionNode playout() {
            boolean consistent = true;
            DecisionNode playoutNode = new DecisionNode(getTotalScore(),getVisits(),getParent(),belief,candidateQueries,unexpandedQueries,depth,reply,children);
            while (!playoutNode.isTerminal() && consistent) {
                Query simulatedQuery = playoutNode.candidateQueries.getRandom();
                User user = sampleUserFromBelief(playoutNode.belief);
                Reply simulatedReply = user.answer(simulatedQuery);
                try {
                    playoutNode.belief.update(simulatedQuery,simulatedReply);
                    playoutNode.candidateQueries.remove(simulatedQuery);
                    playoutNode.depth++;
                } catch (InconsistentBelief inconsistentBelief) {
                    consistent = false;
                }
            }
            return (consistent) ? playoutNode : null;
        }

        public void backup(DecisionNode leafNode) {
            double newScore = recommender.getRecommendationValue(leafNode.belief);
            MCTSNode temporaryNode = this;
            while (temporaryNode != null) {
                temporaryNode.totalScore += newScore;
                temporaryNode.visits++;
                temporaryNode = temporaryNode.parent;
            }
        }

        public Query mostVisitedQuery() {
            int maxVisits = 0;
            Query result = null;
            for (Query query: children.keySet()) {
                ResponseNode responseNode = children.get(query);
                if (responseNode.getVisits() >= maxVisits) {
                    maxVisits = responseNode.getVisits();
                    result = query;
                }
            }
            return result;
        }

        @Override
        public boolean equals(Object o) {
            if (!super.equals(o))
                return false;
            DecisionNode otherNode = (DecisionNode) o;
            return belief.equals(otherNode.belief) && candidateQueries.equals(otherNode.candidateQueries) && unexpandedQueries.equals(otherNode.unexpandedQueries) && depth == otherNode.depth && reply.equals(otherNode.reply) && children.equals(otherNode.children);
        }

        @Override
        public int hashCode() {
            return super.hashCode() ^ belief.hashCode() ^ candidateQueries.hashCode() ^ unexpandedQueries.hashCode() ^ depth ^ reply.hashCode() ^ children.hashCode();
        }

        public boolean isTerminal() {
            return depth >= queriesDepth;
        }

        public double getTotalScore() {
            return super.totalScore;
        }

        public int getVisits() {
            return super.visits;
        }

        public MCTSNode getParent() {
            return super.parent;
        }
    }

    private static class ResponseNode extends MCTSNode {

        private final Query query; //Query the response node refers to

        private final Map<Reply,DecisionNode> children;

        public ResponseNode(Query query, DecisionNode parent) {
            super(parent);
            this.query = query;
            this.children = new HashMap<>();
        }

        //Cloning constructor
        public ResponseNode(double totalScore, int visits, MCTSNode parent, Query query,Map<Reply,DecisionNode> children) {
            super(totalScore,visits,parent);
            this.query = query;
            this.children = children;
        }

        @Override
        public boolean equals(Object o) {
            if (!super.equals(o))
                return false;
            ResponseNode otherNode = (ResponseNode) o;
            return query.equals(otherNode.query) && children.equals(otherNode.children);
        }

        @Override
        public int hashCode() {
            return super.hashCode() ^ query.hashCode() ^ children.hashCode();
        }

        public double getTotalScore() {
            return super.totalScore;
        }

        public int getVisits() {
            return super.visits;
        }
    }

    private static User sampleUserFromBelief(Belief belief) {
        Sampler sampler = belief.sampler();
        double[] sample = sampler.sample();
        return belief.getUser(sample);
    }
}