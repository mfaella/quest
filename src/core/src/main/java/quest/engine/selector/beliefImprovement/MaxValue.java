package quest.engine.selector.beliefImprovement;

import quest.engine.model.*;
import bai.core.*;
import quest.engine.recommender.Recommender;

import java.util.*;

/**
 * This selector identifies the query that maximizes the expected utility of the recommendation.
 *
 * <h3>Dependencies with recommender and answer model</h3>
 * This selector can be used with all answer models and can invoke any type of recommender.
 *
 * <h3>Performance</h3>
 * This selector updates the belief and computes the recommendations for each possible observation (query-reply).
 * For each sample user and each reply, it computes the utility of the corresponding recommendation for that user.
 *
 * <h3>Notes</h3>
 * See EBI_val in the paper.
 */
public class MaxValue implements BeliefImprovement {
	private final static boolean DEBUG = false;

    private Domain domain;

    /* sampleCount is unused because this BeliefImprovement does not perform
       integration directly, but only through the recommender,
       who has its own independent settings. 
    */
    public MaxValue(Domain domain, int sampleCount) {
    	this.domain = domain;
    }

	@Override
	public Function BIFunction(Belief belief, Query q, Recommender recommender) {
		Reply[] replies = q.replies();

		Map<Reply, Item> ev_map = computeRecommendations(belief, q, recommender);

		// System.out.print(".");

		Function.Map voi = (double[] params) -> {
			User u = belief.getUser(params);
			double result = 0.0;
			for(Reply r: replies) {
				double replyProb = u.replyProbability(q, r);
				if(replyProb > 0.0) { //avoid further calculations if the probability of reply is zero
					Item rec = ev_map.get(r);
					double val = (rec != null) ? u.eval(rec) : 0;
					result += replyProb * val;
				}
			}
			return result;
		};
		return Function.make(voi, domain.dim);
	}

	@Override
	public String getTag() {
		return "Val";
	}
}
