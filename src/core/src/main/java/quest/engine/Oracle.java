package quest.engine;

import quest.engine.model.Query;
import quest.engine.model.Reply;

/** A (real or imaginary) user, able to answer to preference elicitation queries.
 */
public interface Oracle {
    Reply answer(Query q);
}
