package quest.engine.attribute;

import java.util.List;

import quest.engine.model.Range;

public abstract class Discrete extends AbstractAttribute {
    private List<String> levels;
    
    public Discrete(String name, List<String> levels, int id) {
	super(name,id);
	this.levels = levels;
    }
    
    @Override
    public boolean hasRange() { return false; }
    @Override
    public Range range() {
	throw new RuntimeException(getClass().getName() + " doesn't have a range!");
    }
    
    @Override
    public String toString(double level) {
	return levels.get((int) (level * levels.size()));
    }
}
