package quest.engine.attribute;

import quest.engine.model.Range;

/** An attribute whose possible levels are all numbers in a given range,
    with an associated unit ("meters", "dollars", etc.).
  */
public abstract class Continuous extends AbstractAttribute {
    private double min, max;
    private String unit;
    
    public Continuous(String name, double min, double max, String unit, int id) {
	super(name,id);
	this.min = min;
	this.max = max;
	this.unit = unit;
    }

    @Override
    public boolean hasRange() { return true; }

    @Override
    public Range range() { return new Range() {
	    public double getMin() { return min; }
	    public double getMax() { return max; }    
	};
    }
        
    @Override
    public String toString(double level) {
	return (min + (level * (max-min))) + " " + unit;
    }
}
