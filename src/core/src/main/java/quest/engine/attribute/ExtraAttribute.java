package quest.engine.attribute;

import quest.engine.model.Range;

// An inactive attribute (corresponds to types Skip and Image)
public class ExtraAttribute extends AbstractAttribute{

    public ExtraAttribute(String nome,int id){
        super(nome,id);
    }

    @Override
    public double eval(double[] param, int offset, double level) {
        return 0;
    }

    @Override
    public int paramCount() {
        return 0;
    }

    @Override
    public String toString(double level) {
        return null;
    }

    @Override
    public boolean hasRange() {
        return false;
    }

    @Override
    public Range range() {
        return null;
    }

    @Override
    public boolean isActive() {
        return false;
    }
}
