package quest.engine.attribute;

public class Parabolic extends Continuous {

    public Parabolic(String name, double min, double max, String unit, int id) {
	super(name, min, max, unit, id);
    }

    @Override
    public double eval(double[] params, int offset, double value){
        double res;
	    final double param = params[offset];
        if (value <= param && param>0)
            res = (2/param)*value - (value*value/(param*param));
        else
            res = (2*param*value +1 -2*param -value*value)/((param-1)*(param-1));
        return res;
    }

    @Override
    public double evalDerivative(double[] params, int offset, double value) {
        double res;
        double param = params[offset];
        if(value <= param && param > 0)
            res = 2/param - (2 * value)/(param * param);
        else
            res = (2*param - 2*value)/((param - 1)*(param - 1));
        return res;
    }

    @Override
    public int paramCount() { return 1; }

    @Override
    public boolean isActive() {
        return true;
    }
}
