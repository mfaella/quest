package quest.engine.attribute;

/** A dummy attribute type whose value is constant.
    Useful for deactivating an attribute.
 */
public class Dummy extends Continuous {

    public Dummy(String name, double min, double max, String unit, int id) {
	super(name, min, max, unit,id);
    }

    @Override
    public double eval(double[] params, int offset, double value){
        return 0;
    }

    @Override
    public double evalDerivative(double[] param, int offset, double level) {
        return 0;
    }

    @Override
    public int paramCount() { return 0; }

    @Override
    public boolean isActive() {
        return true;
    }
}
