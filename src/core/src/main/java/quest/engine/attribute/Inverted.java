package quest.engine.attribute;

import quest.engine.model.Range;

/** 
  */
public class Inverted implements Attribute {

    private final Attribute attr;

    public Inverted(Attribute attr) {
	this.attr = attr;
    }

    public int paramCount() { return attr.paramCount(); }
    public String toString(double level) { return attr.toString(level); }
    public String getName() { return attr.getName(); }
    public int getId() { return attr.getId(); }

    @Override
    public boolean isActive() {
        return true;
    }

    @Override
    public void setIndex(int index) {
        attr.setIndex(index);
    }

    @Override
    public int getIndex() {
        return attr.getIndex();
    }

    public boolean hasRange() { return attr.hasRange(); }
    public Range range() { return attr.range(); }
    
    public double eval(double[] param, int offset, double level) {
        double v = attr.eval(param, offset, level);
        assert (v>=0 && v<=1) : "Inverting incorrect value";
        return 1 - v;
    }

    @Override
    public double evalDerivative(double[] param, int offset, double level) {
        double d = attr.evalDerivative(param, offset, level);
        return -d;
    }
}
