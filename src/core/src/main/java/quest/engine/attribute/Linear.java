package quest.engine.attribute;

public class Linear extends Continuous {

    public Linear(String name, double min, double max, String unit, int id) {
	super(name, min, max, unit, id);
    }

    @Override
    public double eval(double[] params, int offset, double value){
        return value;
    }

    @Override
    public double evalDerivative(double[] param, int offset, double value) {
        return 1;
    }

    @Override
    public int paramCount() { return 0; }

    @Override
    public boolean isActive() {
        return true;
    }
}
