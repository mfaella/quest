package quest.engine.attribute;

public class BoundedSigmoid extends Continuous {

    public BoundedSigmoid(String name, double min, double max, String unit, int id) {
	super(name, min, max, unit,id);
    }

    @Override
    public double eval(double[] params, int offset, double value){
	    final double param = params[offset];
        if (value==1)
            return 1;
        if (param ==0) {
            System.out.println("Warning: Bounded sigmoid evaluated with 0 as parameter, returned value 1");
            return 1;
        }
        if (param ==1) {
            System.out.println("Warning: Bounded sigmoid evaluated with 1 as parameter, returned value 0");
            return 0;
        }
	    double ext_param = (1-param)/param;
        return 1/(1+Math.exp(( (1-value) / (ext_param* value)) - ((ext_param* value) / (1-value)) ));
    }

    @Override
    public double evalDerivative(double[] params, int offset, double value) {
        final double param = params[offset];

        if(param == 0 || param == 1)
            return 0;

        //Derivative at the extremes is zero
        if(value == 0 || value == 1)
            return 0;

        double f = (param * value)/((1 - value)*(1 - param)) - ((1 - value)*(1 - param))/(param * value);
        double f_derivative = param/(Math.pow(1 - value, 2) * (1 - param)) - (param - 1)/(param*value*value);
        return (Math.exp(-f) * f_derivative)/Math.pow(1 + Math.exp(-f), 2);
    }

    @Override
    public int paramCount() { return 1; }

    @Override
    public boolean isActive() {
        return true;
    }
}
