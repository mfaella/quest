package quest.engine.attribute;

import quest.engine.model.Range;

import java.util.List;

/** An attribute in the value model.

    Each item assigns a <i>level</i> to each attribute.
    Each user assigns a set of <i>shape parameters</i> to each attribute.
    Given a level and a set of shape parameters, the <code>eval</code> method of an 
    attribute returns a <i>utility value</i>.
  */
public interface Attribute {

    /** Returns the utility value of this attribute, 
        for a given array of shape parameters and a level.

	The relevant parameters are found in:
	param[offset], param[offset+1], ..., param[offset + paramCount() -1]

	The offset parameter is a performance hack: you can pass an oversized
	array of parameters and read only a part.

	@param param the array of shape parameters
	@param offset the index of the first parameter of this attribute.
	              It must lie in the interval [0, <code>param.length()-1</code>].
	@param level the level of this attribute in a given item
     */

    double eval(double[] param, int offset, double level);

    /** Returns the  first derivative of the eval function at the given point.

     * @param param the array of shape parameters
     * @param offset the index of the first parameter of this attribute
     * @param level the point in which to evaluate the derivative
     */
    double evalDerivative(double[] param, int offset, double level);

    /** Returns the number of shape parameters of this attribute. */
    int paramCount();
    
    /** Converts a canonical level in the [0,1] range to a user-facing level. */
    String toString(double level);

    boolean hasRange();
    Range range();
    
    String getName();

	/** Index of the encoding of the attribute level inside an item object */
    int getId();

    boolean isActive();
    void setIndex(int index);
    int getIndex();

    static int computeDimension(List<Attribute> attribs) {
	    int dim = 0, active = 0;
	    for (Attribute attrib: attribs) {
	        if (attrib.isActive()) {
	            active++;
                dim += attrib.paramCount();
            }
    	}
    	dim += active;
    	return dim;
    }
}
