package quest.engine.attribute;

public class Polynomial extends Continuous {
    public Polynomial(String name, double min, double max, String unit, int id) {
	super(name, min, max, unit, id);
    }

    @Override
    public double eval(double[] params, int offset, double level){
        return Math.pow(level, params[offset]);
    }

    @Override
    public double evalDerivative(double[] params, int offset, double level) {
        double param = params[offset];

        // Here the derivative would be infinity but since 0.0 is
        // a point of minimum in our scenario, we set it to 0.0
        if(level == 0.0)
            return 0.0;
        else
            return param * Math.pow(level, param - 1);
    }

    @Override
    public int paramCount() { return 1; }

    @Override
    public boolean isActive() {
        return true;
    }
}
