package quest.engine.attribute;

public class Triangular extends Continuous {

    public Triangular(String name, double min, double max, String unit, int id) {
	super(name, min, max, unit, id);
    }

    @Override
    public double eval(double[] params, int offset, double level) {
	double param = params[offset];
	if (level <= param)
	    return level/param;
	else
	    return (1-level)/(1-param);
    }

    @Override
    public double evalDerivative(double[] params, int offset, double level) {
        /* La funzione non è derivabile in level == param. Tuttavia, dato che
        * quello è il suo punto di massimo, si potrebbe settare la derivata a 0 */
        double param = params[offset];
        if(level < param)
            return 1/param;
        else if(level == param)
            return 0;
        else
            return -1/(1 - param);
    }

    @Override
    public int paramCount() { return 1; }

    @Override
    public boolean isActive() {
        return true;
    }
}
