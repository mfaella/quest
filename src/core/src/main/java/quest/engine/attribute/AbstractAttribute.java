package quest.engine.attribute;

import quest.engine.attribute.Attribute;

/** An attribute in the value model.

    Each item assigns a <i>level</i> to each attribute.
    Each user assigns a set of <i>shape parameters</i> to each attribute.
    Given a level and a set of shape parameters, the <code>eval</code> method of an 
    attribute returns a <i>utility value</i>.
  */
public abstract class AbstractAttribute implements Attribute {

    private String name;
    private final int id; //the corresponding column in the CSV file of items
    private int index; //index of the corresponding value in levels (> 0 only for active attributes)

    public AbstractAttribute(String name, int id) {
	this.name = name;
	this.id = id;
	this.index = -1; //active attributes will automatically change this value in ItemParser, if the is active
    }

    /** By default an attribute does not support first derivative evaluation. */
    @Override
    public double evalDerivative(double[] param, int offset, double level) {
        throw new UnsupportedOperationException("Derivative is not supported for this attribute.");
    }

    @Override
    public int getId() { return id; }

    @Override
    public String getName() { return name; }

    @Override
    public void setIndex(int index){
        this.index = index;
    }

    @Override
    public int getIndex(){
        return this.index;
    }

}
