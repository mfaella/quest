package quest.engine;

import quest.engine.model.Query;
import quest.engine.model.Reply;

import java.util.Random;

public class RandomOracle implements Oracle {
    private static Random random = new Random();

    @Override
    public Reply answer(Query q) {
	int n = q.replies().length;
	int i = random.nextInt(n);
	return q.replies()[i];
    }
}
