package quest.engine;

import bai.core.Function;
import bai.sampler.Sampler;
import quest.engine.model.Belief;
import quest.engine.model.Domain;
import quest.engine.model.Item;
import quest.engine.model.User;
import quest.engine.recommender.Recommender;
import bai.util.Integral;

import java.util.*;

public class EngineStats {
    private List<Recommender> recommenders;
    private Domain domain;
    private Belief prior;
    private int maxSamples;

    public EngineStats(List<Recommender> recommenders, Domain domain, Belief prior, int maxSamples) {
        this.recommenders = recommenders;
        this.domain = domain;
        this.prior = prior;
        this.maxSamples = maxSamples;
    }

    public void computePriorRecommendations() {
        for (Recommender recommender : this.recommenders) {
            int rec = recommender.getRecommendationIndex(prior);
            System.out.println("Prior recommendation for recommender " + recommender.getTag() +
                    " is item n. " + rec);
        }
    }

    public void computeVarianceOfTop(){
        List<Item> items = this.domain.items;
        Function.Map first = (double[] params) -> {
            User u = prior.getUser(params);
            double topValue = u.getTopItemValue(items);
            return topValue * topValue;
        };

        double expectedValueOfSquaredVariable = Integral.compute(Function.make(first, domain.dim), this.prior,
                this.maxSamples * items.size()).getValue();

        Function.Map second = (double[] params) -> {
            User u = prior.getUser(params);
            return u.getTopItemValue(items);
        };

        double expectedValueOfVariable = Integral.compute(Function.make(second, domain.dim), this.prior,
                this.maxSamples * items.size()).getValue();

        double varianceOfTop = expectedValueOfSquaredVariable - ( expectedValueOfVariable * expectedValueOfVariable );
        System.out.println("Var[u(top)]: "+ varianceOfTop);
    }

    public void computeVarianceOfBottom(){
        List<Item> items = this.domain.items;
        Function.Map first = (double[] params) -> {
            User u = prior.getUser(params);
            double bottomValue = u.getBottomItemValue(items);
            return bottomValue * bottomValue;
        };

        double expectedValueOfSquaredVariable = Integral.compute(Function.make(first, domain.dim), this.prior,
                this.maxSamples * items.size()).getValue();

        Function.Map second = (double[] params) -> {
            User u = prior.getUser(params);
            return u.getBottomItemValue(items);
        };

        double expectedValueOfVariable = Integral.compute(Function.make(second, domain.dim), this.prior,
                this.maxSamples * items.size()).getValue();

        double varianceOfBottom = expectedValueOfSquaredVariable - ( expectedValueOfVariable * expectedValueOfVariable );
        System.out.println("Var[u(bottom)]: "+ varianceOfBottom);
    }

    public void computeDistributionOfTrueTop(){
        Map<Integer, Double> distributionOfTrueTop = new HashMap<Integer, Double>();
        int[] frequencyOfTrueTop = new int[domain.items.size()];
        int userCount = 0;

        Sampler sampler = prior.sampler();
        List<double[]> samples = sampler.sample(maxSamples * domain.items.size());
        for( double[] sample : samples ){
            User user = prior.getUser(sample);
            frequencyOfTrueTop[user.getTopItemIndex(domain.items)]++;
            userCount++;
        }

        for(int i=0; i<domain.items.size(); i++){
            if( frequencyOfTrueTop[i] != 0 ){
                double prob = (double) frequencyOfTrueTop[i] / userCount;
                distributionOfTrueTop.put(i, prob);
            }
        }

        System.out.print("{ ");
        distributionOfTrueTop
                .entrySet()
                .stream()
                .sorted(
                    Comparator.<Map.Entry<?,Double>>comparingDouble(e -> e.getValue()).reversed())
                .forEachOrdered(
                    entry -> System.out.print(entry.getKey() +
                                  ": " + String.format("%.3f", 100 * entry.getValue()) + ", "));
        System.out.println(" }");

        System.out.println("Entropy of the true top distribution: "
                + entropyOfDiscreteDistribution(distributionOfTrueTop.values()));
    }

    public static double entropyOfDiscreteDistribution(Iterable<Double> distrib) {
        double entropy = 0;
        for (Double p: distrib) {
            if (p>0) {
                entropy -= p * Math.log(p); // natural log
            }
        }
        return entropy / Math.log(2); // convert to bits
    }
}
