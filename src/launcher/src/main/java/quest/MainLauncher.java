package quest;

import java.io.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.lang.reflect.InvocationTargetException;
import org.json.simple.parser.ParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.ValidationMessage;

import quest.engine.*;
import quest.einterface.*;
import quest.engine.attribute.Attribute;
import quest.engine.model.*;

public class MainLauncher
{

	public static void main(String[] args) throws NoSuchMethodException,
			SecurityException, InstantiationException, IllegalAccessException,
			InvocationTargetException, ParseException {
		String statsValue = null;
		String jsonValue = null;
		String cvsItems = null;
		String jsonConfig = null;

		if (args.length < 3) {
			System.out.println("Error: Incomplete input. Usage:");
			System.out.println("   quest <attributes> <items> <config>\n");
			System.out.println("1 - Attributes: Value model (json)");
			System.out.println("2 - Items: Item list (csv)");
			System.out.println("3 - Config: Elicitation configuration (json)");
			System.out.println("\n");
			System.exit(1);
		} else {
			if ( args.length == 4 ) {
				statsValue = args[0];
				jsonValue = args[1]; // Value model (attributes)
				cvsItems = args[2];  // List of items
				jsonConfig = args[3]; // Elicitation config
			} else {
				jsonValue = args[0];
				cvsItems = args[1];
				jsonConfig = args[2];
			}
		}


		String errorFilename = ""; // initialize to prevent "may not have been initialized" error

		try {
			//STATS VALIDATION
			Boolean statsFlag = false;
			if ( statsValue != null ){
				if ( statsValue.equals("-stats") ) {
					statsFlag = true;
				} else {
					System.out.println("[STATS COMMAND VALIDATION FAILED]");
					System.out.println("Unknown command");
					System.exit(1);
				}
			}

			//CONFIG JSON VALIDATION
			errorFilename = jsonConfig;
			InputStream configStream = new FileInputStream(jsonConfig);
			ObjectMapper configMapper = new ObjectMapper();
			JsonNode configNode = configMapper.readTree(configStream);

			//String schemaName = "/home/mfaella/Dev/quest/src/schema.json";
			errorFilename = "config-schema.json";
			InputStream configSchemaStream = new FileInputStream("config-schema.json");
			JsonSchemaFactory configFactory = JsonSchemaFactory.getInstance();
			JsonSchema configSchema = configFactory.getSchema(configSchemaStream);

			Set<ValidationMessage> configErrors = configSchema.validate(configNode);

			if (!configErrors.isEmpty()) {
				System.out.println("[CONFIG FILE VALIDATION FAILED]");
				System.out.println(configErrors);
				System.exit(1);
			} else
				System.out.println("[CONFIG FILE VALIDATED]");

			//ATTRIBS JSON VALIDATION
			errorFilename = jsonValue;
			InputStream attribsStream = new FileInputStream(jsonValue);
			ObjectMapper attribsMapper = new ObjectMapper();
			JsonNode attribsNode = attribsMapper.readTree(attribsStream);

			//String schemaName = "/home/mfaella/Dev/quest/src/schema.json";
			errorFilename = "attribs-schema.json";
			InputStream attribsSchemaStream = new FileInputStream("attribs-schema.json");
			JsonSchemaFactory attribsFactory = JsonSchemaFactory.getInstance();
			JsonSchema attribsSchema = attribsFactory.getSchema(attribsSchemaStream);

			Set<ValidationMessage> attribsErrors = attribsSchema.validate(attribsNode);

			if (!attribsErrors.isEmpty()) {
				System.out.println("\n[ATTRIBS FILE VALIDATION FAILED]");
				System.out.println(attribsErrors);
				System.exit(1);
			} else
				System.out.println("[ATTRIBS FILE VALIDATED]");

			// Attributes
			AttributeParser attributeParser = new AttributeParser(jsonValue);
			List<Attribute> attributes = attributeParser.generateAttribute();

			// Items
			ItemParser itemParser = new ItemParser(cvsItems, attributes);
			List<Item> items = itemParser.generateItem();

			// Elicitation Domain
			Domain domain = new Domain(attributes, items);

			// Elicitation Experiment
			Experiment experiment = new Experiment(domain, statsFlag);

			//CONFIG EXPERIMENT
			ConfigParser configParser = new ConfigParser(jsonConfig);
			int dim = Attribute.computeDimension(attributes);
			configParser.configure(experiment,dim);
			System.out.println(" Experiment ready to run ... \n");

			// Elicitation run
			experiment.run();

		} catch (IOException ex) {
			System.out.println("Error: " + ex.getMessage() + " in file " + errorFilename);
			System.exit(1);
		}
	}
}
