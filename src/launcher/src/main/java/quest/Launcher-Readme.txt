Launcher configuration files:
- priorModel.json: json specification of the prior model
- valueModel.json: json specification of the prior model (attributes)
- itemList.csv: cvs specification of the list of items
- oracle-interactive.json: example of the specification of the interactive system
- oracle-batch-normal.json: example of the specification of a batch experiment (with normal distribution)

Laucher Classes:
- MainLauncher: main for launcher (example of an interactive experiment)
- JsonParserPriorModel: parser of the prior model
- JsonParserValueModel: parser of the value model
- ItemGenerator: generation of the items
- ConfigElicitation: configuration of the elicitation engine
- OracleModel: configuration of the oracle (interactive vs batch) and specification of the batch experiment

Launcher compile:   
mvn compile -pl launcher (see also the associated pom.xml) 

Launcher exec:  
mvn exec:java -pl launcher