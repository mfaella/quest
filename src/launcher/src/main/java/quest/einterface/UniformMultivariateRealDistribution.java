package quest.einterface;

import org.apache.commons.math3.distribution.*;
import org.apache.commons.math3.random.*;

public class UniformMultivariateRealDistribution
    extends AbstractMultivariateRealDistribution {
   
    public UniformMultivariateRealDistribution(int dim) {
	super(new JDKRandomGenerator(), dim);
    }

    @Override
    public double[] sample() {
	final int dim = getDimension();
	double[] sample = new double[dim];
	for (int i=0; i<dim; i++)
	    sample[i] = random.nextDouble();
	return sample;		
    }
    
    @Override
    public double density(double[] point) {
	return 1;
    }
    
    @Override
    public UniformMultivariateRealDistribution clone() {
	final int dim = getDimension();
	UniformMultivariateRealDistribution copy =
	    new UniformMultivariateRealDistribution(dim);
	return copy;
    }
}
