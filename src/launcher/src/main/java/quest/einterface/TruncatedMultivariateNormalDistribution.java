package quest.einterface;

import java.util.Arrays;

import bai.core.*;
import bai.util.*;

import org.apache.commons.math3.random.*;
import org.apache.commons.math3.distribution.*;

/** A multivariate normal distribution truncated to the hypercube [0,1]^d.
 */
public class TruncatedMultivariateNormalDistribution extends AbstractMultivariateRealDistribution {
    private final MultivariateNormalDistribution normal;
    private final double centralMass;
    private final int dim;
    private static final int MAXSAMPLES = 10000;
    
    public TruncatedMultivariateNormalDistribution(double[] means, double[][] cov) {
	super(new JDKRandomGenerator(), means.length);
	dim = means.length;
	normal = new MultivariateNormalDistribution(means, cov);

	double[] lower = new double[dim], upper = new double[dim];
	Arrays.fill(upper, 1);
	Result centralMassResult = Integral.compute(Function.make(normal::density, dim),
						    new CustomDensity(Function.make(p -> 1, dim), lower, upper),
						    MAXSAMPLES);
	centralMass = centralMassResult.getValue();
	// DEBUG
	// System.out.println("** centralMass =" + centralMass);
    }
			
    @Override
    public double[] sample() {
	int i;
	while (true) {
	    double[] sample = normal.sample();
	    for (i=0; i<dim; i++)
		if (sample[i]<0 || sample[i]>1)
		    break; // reject this sample
	    if (i==dim)
		return sample;
	}
    }
    
    @Override
    public double density(double[] point) {
	return normal.density(point) / centralMass;
    }

    @Override
    public TruncatedMultivariateNormalDistribution clone() {
	TruncatedMultivariateNormalDistribution copy =
	    new TruncatedMultivariateNormalDistribution(this.normal.getMeans(),
							this.normal.getCovariances().getData());
	    return copy;
    }
}
