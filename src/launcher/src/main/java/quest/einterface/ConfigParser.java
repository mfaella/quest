package quest.einterface;
 
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
 
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import quest.engine.*;
import quest.engine.filter.QueryFilter;
import quest.engine.model.AnswerModel;
import quest.engine.model.Belief;
import quest.engine.model.Domain;
import quest.engine.selector.beliefImprovement.*;
import quest.engine.selector.*;
import quest.engine.recommender.*;
import quest.engine.util.BAI;

public class ConfigParser {

	private String filename;
	private String errorcheck;

	public ConfigParser(String name) {
		this.filename = name;
	}

	private static final String SAMPLE_COUNT = "Samples";
	private static final String MAX_FINDER = "MaxFinder";

	public void configExperiment(Experiment experiment) throws ParseException {

		try {
			FileReader reader = new FileReader(this.filename);

			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);

			// EXPERIMENT DIM and DOMAIN
			int dim = experiment.dim();
			Domain domain = experiment.getDomain();

			// READ THE CONFIG PART
			String outputName = (String) jsonObject.get("Output");
			if (outputName == null) {
				outputName = "test.csv";
			}
			experiment.setBatchOutputName(outputName);

			// SAMPLES
			int sampleCount;
			if (jsonObject.get(SAMPLE_COUNT) != null) {
				sampleCount = (int) (long) jsonObject.get(SAMPLE_COUNT);
			} else {
				sampleCount = 10000;
			}
			experiment.setMaxSamples(sampleCount);
			System.out.println("\nNumber of samples: " + sampleCount);

			// MAX FINDER
			String maxFinderString = "SmartMaxFinder";
			if (jsonObject.get(MAX_FINDER) != null) {
				maxFinderString = jsonObject.get(MAX_FINDER).toString();
				switch (maxFinderString){
					case "SmartMaxFinder":
						BAI.setMaxFinderEnumeration(BAI.maxFinderEnumeration.SmartMaxFinder);
						break;
					case "SimpleMaxFinder":
						BAI.setMaxFinderEnumeration(BAI.maxFinderEnumeration.SimpleMaxFinder);
						break;
					case "SRMaxFinder":
						BAI.setMaxFinderEnumeration(BAI.maxFinderEnumeration.SRMaxFinder);
						break;
					case "SHMaxFinder":
						BAI.setMaxFinderEnumeration(BAI.maxFinderEnumeration.SHMaxFinder);
						break;
				}
			} else {
				BAI.setMaxFinderEnumeration(BAI.maxFinderEnumeration.SmartMaxFinder);
			}

			System.out.println("\nMax Finder: " + maxFinderString);

			boolean explicitRecommendersSelectors = parseRecommendersAndSelectors(experiment, jsonObject, domain,
					sampleCount);

			//Set explicitRecommendersSelectors variable
			experiment.setExplicitRecommendersSelectors(explicitRecommendersSelectors);

			if(!explicitRecommendersSelectors) {
				System.out.println("ExplicitRecommendersSelectors field is null");
				System.out.println("Doing parsing of BI and Recommenders...");
				parseRecommenders(experiment, jsonObject, domain, sampleCount);
				parseSelectors(experiment, jsonObject, domain, sampleCount);
			}
			parseFilters(experiment, jsonObject, domain);
			parseAnswerModel(experiment, jsonObject);

			//Set number of queries
			int nQueries = (int) (long) jsonObject.get("NumberOfQueries");
			System.out.println("\nNumber of queries: " + nQueries);

			final int numberOfQueries = nQueries;
			experiment.setNumberOfQueries(numberOfQueries);

			//Set viewRanksOfQuery variable
			Object temporaryVariable = jsonObject.get("ViewRanksOfQuery");
			boolean viewRanksOfQuery = temporaryVariable != null && (boolean) temporaryVariable;
			System.out.println("\nExperiment with ranking of query's items: "+viewRanksOfQuery);
			experiment.setViewRanksOfQuery(viewRanksOfQuery);

		} catch (Exception e) {
			System.out.println("Error: " + this.errorcheck + " not valid in config file");
			System.out.println(e);
			System.exit(1);
			//throw new RuntimeException(this.errorcheck + " non valido nel file config");
		}
	}

	private void parseAnswerModel(Experiment experiment, JSONObject jsonObject) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
		String typeOfAnswerModel;
		List<Double> params = new ArrayList<>();
		if(jsonObject.get("AnswerModel") != null) {
			JSONObject structure = (JSONObject) jsonObject.get("AnswerModel");
			typeOfAnswerModel = (String) structure.get("Type");
			JSONArray paramsJsonArray = (JSONArray) structure.get("Params");
			for(int i = 0; i < paramsJsonArray.size(); i++) {
				Double param = (Double) paramsJsonArray.get(i);
				params.add(param);
			}
		}
		else {
			typeOfAnswerModel = "DeterministicAnswerModel";
			params.add(0.005);
		}

		this.errorcheck = "AnswerModel " + typeOfAnswerModel;
		Class<?> aw = Class.forName("quest.engine.model." + typeOfAnswerModel);
		Class[] paramType = new Class[params.size()];
		for(int i = 0; i < paramType.length; i++)
			paramType[i] = double.class;
		this.errorcheck = "AnswerModel number of parameters";
		Constructor<?> awnew = aw.getConstructor(paramType);
		AnswerModel answerModelObject = (AnswerModel) awnew.newInstance(params.toArray());
		experiment.getDomain().answerModel = answerModelObject; //maybe better to use a setter
	}

	private void parseSelectors(Experiment experiment, JSONObject jsonObject, Domain domain, int sampleCount) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
		List<String> typeOfBI = new ArrayList<>();
		JSONArray jArray = (JSONArray) jsonObject.get("BI");
		if (jArray != null) {
			for (int i = 0; i < jArray.size(); i++) {
				String bielement = (String) jArray.get(i);
				typeOfBI.add(bielement);
			}
		} else {
			typeOfBI.add("MinTopEntropy");
		}
		this.createSelectorsAndAddToExperiment(experiment, typeOfBI, domain, sampleCount);
	}

	private void parseFilters(Experiment experiment, JSONObject jsonObject, Domain domain) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
		this.errorcheck = "Filters";
		List<String> filters = new ArrayList<>();
		JSONArray jArrayFlt = (JSONArray) jsonObject.get("Filters");
		if (jArrayFlt != null) {
			for (Object o: jArrayFlt) {
				String filter = (String) o;
				filters.add(filter);
			}
		} else {
			filters.add("All");
		}
		int num = 1;
		for (String typeOfFlt : filters) {
			this.errorcheck = "Filter " + typeOfFlt;
			System.out.println(num + " - Filter: " + typeOfFlt);
			num++;
			Class<?> c = Class.forName("quest.engine.filter." + typeOfFlt);
			Class[] paramType = {Domain.class};
			Constructor<?> cnew = c.getConstructor(paramType);
			QueryFilter qfilter = (QueryFilter) cnew.newInstance(domain);
			experiment.addQueryFilter(qfilter);
		}
	}

	private void createRecommendersAndAddToExperiment(Experiment experiment, List<String> listRecommenders,
													  Domain domain, int sampleCount) throws NoSuchMethodException,
			InvocationTargetException, IllegalAccessException {
		int num = 1;
		for (String typeOfRec : listRecommenders) {
			this.errorcheck = "Recommender " + typeOfRec;
			System.out.println(num + "- Recommender: " + typeOfRec);
			num++;
			// invoke constructor
			Class[] paramType = {Domain.class, int.class};
			Method recMethod = Recommenders.class.getDeclaredMethod("get" + typeOfRec, paramType);
			Recommender recommender = (Recommender) recMethod.invoke(null, domain, sampleCount);
			experiment.addRecommender(recommender);
		}
	}
	
	private void createSelectorsAndAddToExperiment(Experiment experiment, List<String> listSelectors, Domain domain,
												   int sampleCount) throws ClassNotFoundException,
			NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		System.out.println("\n" + listSelectors.size() + " BIs: ");
		int num = 1;
		for (String bi : listSelectors) {
			System.out.println(num + " - " + bi);
			num++;
			this.errorcheck = "BeliefImprovement " + bi;
			Class<?> v;
			try {
				v = Class.forName("quest.engine.selector." + bi);
			} catch (ClassNotFoundException e) {
				v = Class.forName("quest.engine.selector.beliefImprovement." + bi);
			}

			Class[] paramType = {Domain.class, int.class};
			Constructor<?> vnew = v.getConstructor(paramType);
			//System.out.println(" check BI \n");  // check constr. of Standard, diff paramType
			if (Arrays.asList(v.getInterfaces()).contains(BeliefImprovement.class)) {
				BeliefImprovement beliefImprovementObject = (BeliefImprovement) vnew.newInstance(domain, sampleCount);
				QuerySelector querySelector = new Standard(beliefImprovementObject, sampleCount);
				experiment.addQuerySelector(querySelector);
			} else { // It's a querySelector
				QuerySelector querySelector = (QuerySelector) vnew.newInstance(domain, sampleCount);
				experiment.addQuerySelector(querySelector);
			}
		}
	}

	private boolean parseRecommendersAndSelectors(Experiment experiment, JSONObject jsonObject, Domain domain,
												  int sampleCount) throws ClassNotFoundException, NoSuchMethodException,
			IllegalAccessException, InvocationTargetException, InstantiationException {
		List<String> listRecommenders = new ArrayList<>();
		List<String> listSelectors = new ArrayList<>();
		JSONArray jArrayRecSel = (JSONArray) jsonObject.get("ExplicitRecommendersSelectors");
		if(jArrayRecSel == null) {
			return false;
		}
		System.out.println("Skipped parsing of BI and Recommenders");
		System.out.println("Doing parsing of ExplicitRecommendersSelectors...");

		for (int i = 0; i < jArrayRecSel.size(); i++) {
			JSONArray recommenderSelector = (JSONArray) jArrayRecSel.get(i);
			listRecommenders.add((String) recommenderSelector.get(0));
			listSelectors.add((String) recommenderSelector.get(1));
		}

		this.createRecommendersAndAddToExperiment(experiment, listRecommenders, domain, sampleCount);
		this.createSelectorsAndAddToExperiment(experiment, listSelectors, domain, sampleCount);
		return true;
	}

	private void parseRecommenders(Experiment experiment, JSONObject jsonObject, Domain domain, int sampleCount) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		List<String> listRecommenders = new ArrayList<>();
		JSONArray jArrayRec = (JSONArray) jsonObject.get("Recommenders");
		if (jArrayRec != null) {
			for (int i = 0; i < jArrayRec.size(); i++) {
				String recElement = (String) jArrayRec.get(i);
				listRecommenders.add(recElement);
			}
		} else {
			listRecommenders.add("MaxValue");
		}
		this.createRecommendersAndAddToExperiment(experiment, listRecommenders, domain, sampleCount);
	}

	private void config(Experiment experiment) throws ParseException, IOException {
		this.configExperiment(experiment);

		OracleParser oracleParser = new OracleParser(this.filename);
		oracleParser.config(experiment);
	}

	public void configure(Experiment experiment, int dim) throws IOException, ParseException{
		config(experiment);
		PriorParser priorParser = new PriorParser(this.filename, experiment.getDomain().answerModel);
		Belief belief = priorParser.JsonParseBelief(dim);;
		experiment.setPrior(belief);
	}

}

