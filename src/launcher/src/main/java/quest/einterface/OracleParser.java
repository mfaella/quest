package quest.einterface;
 
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.*;
import quest.engine.*;
import org.apache.commons.math3.distribution.*;

 
public class OracleParser {
	
    private String filename;
    private String errorcheck;
     
    public OracleParser(String name) {
    	this.filename = name;
    }

	public void config(Experiment experiment) throws ParseException, IOException {

		FileReader reader = new FileReader(this.filename);

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);

		//System.out.println("Oracle Model Configuration ... ");
		JSONObject structure = (JSONObject) jsonObject.get("Interaction");

		int dim = experiment.dim();

		String typeInteraction;
		if (structure != null) {
			typeInteraction = (String) structure.get("Type");

			if (typeInteraction == null) {
				typeInteraction = "InteractiveExperiment";
			}
		} else {
			typeInteraction = "InteractiveExperiment";
		}

		System.out.println("\nOracle: " + typeInteraction);

		switch (typeInteraction) {
			case "InteractiveExperiment":
				// OLD Version: the new one?
				// Set interactive oracle
				//el.setExperiment(0);
				experiment.setOracleModel(InteractiveOracle.singleton());
				experiment.setNumberOfThreads(1);
				break;

			case "BatchExperiment":
				int nThreads = 1;
				int NumberOfUsers = 0;

				Object concurrency = structure.get("Concurrency");
				if (concurrency != null)
					nThreads = (int) (long) concurrency;

				System.out.println("\nNumber of threads: " + nThreads);

				final int numberOfThreads = nThreads;
				experiment.setNumberOfThreads(numberOfThreads);

				NumberOfUsers = (int) (long) structure.get("NumberOfUsers");
				if (NumberOfUsers == 0) // Default
					NumberOfUsers = 100;
				experiment.setNumberOfUsers(NumberOfUsers);

				JSONObject generatorstructure = (JSONObject) structure.get("Generator");

				String typeDensity;
				if (generatorstructure != null) {
					typeDensity = (String) generatorstructure.get("Type");
					if (typeDensity == null) {
						typeDensity = "Uniform";
					}
				} else {
					typeDensity = "Uniform";
				}

				MultivariateRealDistribution userDensity = null;

				// We can cut this:
				//System.out.println("Parser PM: Uniform Dis dim: " + generatorstructure.get("dim"));
				//double[] lowerBounds = new double[dim], upperBounds = new double[dim];
				//Arrays.fill(upperBounds, 1);

				// Set batch oracle model
				switch (typeDensity) {
					case "Normal":
						userDensity = DensityParser.parseNormal(dim, generatorstructure);
						break;
					case "Uniform":
						userDensity = DensityParser.parseUniform(dim, generatorstructure);
						break;
					default:
						System.out.println("\n");
						System.out.println("Error: " + typeDensity + " is an unrecognized generator probability density in config file.");
						System.exit(1);
				}
				experiment.setOracleModel(
						new BatchExperiment(userDensity, experiment.getDomain().answerModel, NumberOfUsers));
				break;
			default:
				// Set interactive oracle
				//el.setOracleModel(InteractiveOracle.singleton());
				System.out.println("\n");
				System.out.println("Error: " + typeInteraction + "is an unrecognized experiment type in config file.");
				System.exit(1);
		}

		//long nSamples =  (long) jsonObject.get("VOIsamples");
		//System.out.println("The number of samples is: " + nSamples);

		//} catch (Exception e) {
		//throw new RuntimeException(e);
		//}

	/* catch (FileNotFoundException ex) {
	   ex.printStackTrace();
	   } catch (IOException ex) {
	   ex.printStackTrace();
	   } catch (ParseException ex) {
	   ex.printStackTrace();
	   } catch (NullPointerException ex) {
	   ex.printStackTrace();
	   }*/

	}

}

