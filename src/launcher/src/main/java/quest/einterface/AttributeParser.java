package quest.einterface;

import java.util.*;

import quest.engine.attribute.Attribute;
import quest.engine.attribute.Inverted;

import java.io.FileReader;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
 
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
 
public class AttributeParser {

    private static final String SKIP = "Skip";
    private static final String IMAGE = "Image";
    private String filename;
    
    public AttributeParser(String name) {
    	this.filename = name;
    }
    
    public List<Attribute> generateAttribute() throws ParseException, NoSuchMethodException, 
						      SecurityException, InstantiationException, IllegalAccessException,
						      IllegalArgumentException, InvocationTargetException {
    	
    	List<Attribute> attributes = new ArrayList<Attribute>();
    	
    	try {
    		FileReader reader = new FileReader(filename);
            
	  	  	JSONParser jsonParser = new JSONParser();
	  	  	JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
            
	  	  	//System.out.println("Parsing Value Model ... ");
			JSONObject structure = (JSONObject) jsonObject.get("valueModel");
            
            // get an array from the JSON object
            JSONArray listAttributes = (JSONArray) structure.get("attributesList");
    		
	    	System.out.println("\nATTRIBUTE LIST: ");
			int k=0; //used as index for array levels in Item class
            for (int i=0; i<listAttributes.size(); i++){
                JSONObject structureAttr = (JSONObject) listAttributes.get(i);
                String nameAttr = (String) structureAttr.get("attribute-name");
                String classAttr = (String) structureAttr.get("attribute-class");
                System.out.println((i+1) + " - " + nameAttr + " (" + classAttr + ")");

				if (classAttr.equals(SKIP) || classAttr.equals(IMAGE)) {
			    	//Eliminare riscrittura di codice
					Class[] paramTypes = {String.class,int.class};
					Attribute attr = null;
					try {
						Class<?> c = Class.forName("quest.engine.attribute.ExtraAttribute");
						Constructor<?> cnew = c.getConstructor( paramTypes );
						attr = (Attribute) cnew.newInstance(nameAttr, i);
					} catch (ClassNotFoundException e) {
						System.out.println("Error: can't find attribute class ExtraAttribute");
						System.exit(1);
					}
					attributes.add(attr);
			    	continue;
				}
		
				JSONObject paramAttr = (JSONObject) structureAttr.get("attribute-param");
				double min = (double) paramAttr.get("min");
				double max = (double) paramAttr.get("max");
				String unit = (String) paramAttr.get("dim");

				// System.out.println(" [min=" + min + " |  max=" + max + "]");
		
				boolean inverted = false;
				// System.out.println(structureAttr.get("attribute-invert"));
				if (structureAttr.get("attribute-invert")!=null &&
				    structureAttr.get("attribute-invert").equals("true")) {
				    inverted = true;
				    // System.out.println("inverted!");
				}
		
				Class[] paramTypes = { String.class, double.class, double.class, String.class, int.class };
				Attribute attr = null;
				try {
			    	Class<?> c = Class.forName("quest.engine.attribute." + classAttr);
			    	Constructor<?> cnew = c.getConstructor( paramTypes );
			    	attr = (Attribute) cnew.newInstance(nameAttr, min, max, unit, i);
					attr.setIndex(k++);
				} catch (ClassNotFoundException e) {
		    		System.out.println("Error: can't find attribute class " + classAttr);
					System.exit(1);
				}
				if (inverted)
		    		attr = new Inverted(attr);
					attributes.add(attr);
            }
        } catch (Exception e) {
			System.out.println("\nError: wrong keywords in attribute file");
			System.out.println(e);
			System.exit(1);
		}
		
    	return attributes;
    }
 
}

