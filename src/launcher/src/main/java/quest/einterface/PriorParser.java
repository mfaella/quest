package quest.einterface;

import org.apache.commons.math3.distribution.*;

import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import quest.engine.model.*;

// Parser of the Prior Model 
public class PriorParser {

	private final String filename;
	private final AnswerModel answerModel;

	public PriorParser(String name, AnswerModel answerModel) {
		this.filename = name;
		this.answerModel = answerModel;
	}

	public Belief JsonParseBelief(final int dim) throws ParseException, IOException {

		FileReader reader = new FileReader(this.filename);

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
		JSONObject structure = (JSONObject) jsonObject.get("Prior");

		String typeDensity = null;
		if (structure != null) {
			typeDensity = (String) structure.get("Type");
		}
		if (typeDensity == null) { // The default prior
			typeDensity = "Uniform";
		}

		System.out.println("\nPrior model: " + typeDensity + " (dim: " + dim + ")");

		MultivariateRealDistribution mrd = null;
		// Switch depending on the density type
		switch (typeDensity) {
			case "Normal" :
				mrd = DensityParser.parseNormal(dim, structure);
				break;
			case "Uniform":
				mrd = DensityParser.parseUniform(dim, structure);
				break;
			default:
				System.out.println("\n");
				System.out.println("Error: " + typeDensity + " is an unrecognized prior probability density.");
				System.exit(1);
		}
		// Change this line to choose the type of belief
		return new CleanUpdateBelief(mrd, answerModel);
	}
}

