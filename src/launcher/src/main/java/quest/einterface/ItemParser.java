package quest.einterface;

//package quest.engine;

import java.util.*;

import quest.engine.attribute.Attribute;

import com.opencsv.CSVReader;
import quest.engine.model.Item;
import quest.engine.model.Range;

import java.io.FileReader;
import java.io.IOException;


public class ItemParser {
	
    private String filename;
    private List<Attribute> attributes;
	private List<Attribute> activeAttributes;
    public ItemParser(String name, List<Attribute> attributes) {
    	/*Create a list containing only active attributes*/
		activeAttributes = new ArrayList<Attribute>();
    	for(Attribute attr: attributes){
    		if(attr.isActive()){
    			activeAttributes.add(attr);
			}
		}
		this.filename = name;
		this.attributes = attributes;
    }
    
    public List<Item> generateItem() {
    	
    	List<Item> items = new ArrayList<>();
    	
    	try {
            CSVReader reader = new CSVReader(new FileReader(this.filename));
            String[] line;
            
			int j=1;
            while ((line = reader.readNext()) != null) {
                // System.out.println("[GenItem] item [name= " + line[0] + ", att1= " + line[1] + " , att2=" + line[2] + "]");
                Item item = new Item(line[0], attributes,activeAttributes);

				int i=0; //used as index for csv columns
				for (Attribute attr: attributes) {
		    		double attrValue = 0;
		   		 	try {
		   		 		i=attr.getId()+1;

		   		 		if(!attr.isActive()){
							item.addExtraAttribute(attr.getName(),line[i]);
							continue;
						}else {
							//System.out.println("Attribute "+attr.getName()+" at column "+i);
							attrValue = Double.parseDouble(line[i]);

							if (attr.hasRange()) { // Normalize
								Range range = attr.range();
								attrValue = (attrValue - range.getMin()) / (range.getMax() - range.getMin());
								//if (attrValue < 0 || attrValue >1) {
								//	System.out.println(Double.parseDouble(line[i]) + ", " + attrValue + ": " + range.getMin() + ", " + range.getMax());
								//}
							}
							// System.out.println("attr: " + attr + "(" + attrValue + ")");
							item.setAttributeLevel(attrValue,attr.getIndex());
						}
		    		} catch (NumberFormatException e) {
						System.out.println("\nError: wrong value in " + this.filename + ": LINE " + j + ", COLUMN " +i);
						System.exit(1);
		    		}
					catch (ArrayIndexOutOfBoundsException e) {
						int missing = attributes.size()+1-i;
						System.out.println("\nError: in file " + this.filename + ", " + missing + " values are missing in column "+i);
						System.exit(1);
		    		}

				}
				items.add(item);
				j++;
            }
	    
	    	System.out.println("\nNumber of items: " + items.size());
    	} catch (IOException e) {
            throw new RuntimeException("Errore di parsing nel file items", e);
        }
    	
    	return items;    	
    }        
}

