package quest.einterface;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import org.apache.commons.math3.distribution.*;

public class DensityParser {
    public static MultivariateRealDistribution parseNormal(int dim, JSONObject densityJSON) {
	JSONArray distMean = (JSONArray) densityJSON.get("Mean");

	if (distMean==null)
	    throw new RuntimeException("Missing mean vector (key 'Mean')");
	
	// Mean vector
	double[] jsMean = new double[dim];
	for (int j = 0; j < dim; j++) {
	    double f = (double) distMean.get(j);
	    jsMean[j] = f;
	}
        
	// Covariance matrix
	JSONArray distCov = (JSONArray) densityJSON.get("Cov");

	if (distCov==null)
	    throw new RuntimeException("Missing covariance martrix (key 'Cov')");
	
	double[][] covMatrix = new double[dim][dim]; 
	// take the elements of the json array
	for(int i=0; i< dim; i++){
	    
	    JSONArray distCovLine = (JSONArray) distCov.get(i);
	    for (int j = 0; j < dim; j++) {
		double f = (double) distCovLine.get(j);
		covMatrix[i][j] = f;
	    } 
	}
                
	return new TruncatedMultivariateNormalDistribution(jsMean, covMatrix);
    }

    // A uniform distribution over the hyper-cube [0,1]^dim.
    public static MultivariateRealDistribution parseUniform(int dim, JSONObject densityJSON) {
	return new UniformMultivariateRealDistribution(dim);
    }
}
